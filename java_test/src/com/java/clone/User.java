package com.java.clone;

public class User implements Cloneable{
	private int id ;
	private Sex sex ;
	
	@Override
	protected User clone() throws CloneNotSupportedException {
		return (User) super.clone();
	}
	
	@Override
	public String toString() {
		return super.toString()/*"id = " + id + ", sex = " + (sex != null ? sex.name():"")*/;
	}
	
	public static void main(String[] args) throws CloneNotSupportedException {
		User user1 = new User() ;
		user1.id = 1 ;
		user1.sex = Sex.MALE ;
		
		System.out.println("1" + user1.toString());
		
		System.out.println("copy"+ user1.clone());
	}
}
