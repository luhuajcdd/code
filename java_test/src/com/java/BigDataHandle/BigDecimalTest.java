package com.java.BigDataHandle;
import java.math.BigDecimal;


public class BigDecimalTest {

    /**
     * @Description: TODO
     * @param @param args 
     * @return 
     * @throws
     */
    public static void main(String[] args) {
	String numS = "1110.455557777777775" ;	
	BigDecimal b = new BigDecimal(numS);
	System.out.println(b.setScale(7, BigDecimal.ROUND_HALF_UP).toString());
	System.out.println(b.setScale(2, BigDecimal.ROUND_HALF_UP).floatValue());
	System.out.println(b.setScale(7, BigDecimal.ROUND_HALF_UP).doubleValue());
    }

}
