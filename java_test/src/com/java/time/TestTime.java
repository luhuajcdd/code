package com.java.time;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class TestTime {
	public static void main(String[] args) throws IOException, ParseException {
		
		TestTime time = new TestTime() ;
		time.getCurrentDayZeroMillis() ;
		time.getDayEndMillis() ;
		//timeChansfer();
		
	}

	private static void timeChansfer() throws ParseException {
		TestTime test = new TestTime();
		Date fromDate = Calendar.getInstance().getTime();
		System.out.println("local Time - " + fromDate);
		System.out.println("GMT Time - " + test.cvtToGmt(fromDate));
		System.out.println();
		System.out.println();
		System.out.println();
		System.out.println();

		
		System.out.println(test.gmtToLocal2(fromDate.getTime()));
	
		String[] ids = TimeZone.getAvailableIDs() ;
		for(String di : ids){
			//System.out.println(di);
		}
		
		System.out.println("start = "+test.gmtToLocal2(0));
		
		System.out.println(System.currentTimeMillis());
		System.out.println("start = "+test.gmtToLocal2(System.currentTimeMillis()));
		long cur = test.gmtToLocal2(60000) ;
		System.out.println("cur = " + cur);
		long cur2 = test.gmtToLocal2(0) ;
		System.out.println("cur2 = " + cur2);
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss") ;
		System.out.println(format.format(new Date(cur)));
		
		System.out.println(format.format(new Date(cur2)));
		
		Date date = new Date(cur2) ;
		Calendar calendar = Calendar.getInstance() ;
		calendar.setTime(date) ;
		System.out.println(calendar.getTimeInMillis()) ;
		System.out.println("date = "+new Date(cur2));
		System.out.println("date = "+new Date(60000));

		String[] timeZones =TimeZone.getAvailableIDs() ;
		for(String timeZone : timeZones ){
			System.out.println(timeZone);
		}
	}
	
	private Date cvtToGmt(Date date) {
		TimeZone tz = TimeZone.getDefault();
		Date ret = new Date(date.getTime() - tz.getRawOffset());
		
		// if we are now in DST, back off by the delta. Note that we are
		// checking the GMT date, this is the KEY.
		if (tz.inDaylightTime(ret)) {
			Date dstDate = new Date(ret.getTime() - tz.getDSTSavings());
			
			// check to make sure we have not crossed back into
			// standard time
			// this happens when we are on the cusp of DST (7pm the
			// day before the change for PDT)
			if (tz.inDaylightTime(dstDate)) {
				ret = dstDate;
			}
		}
		return ret;
	}
	
	private long gmtToLocal2(long gmtMillis) throws ParseException{
		TimeZone timeZone = Calendar.getInstance().getTimeZone() ;
		return gmtMillis + timeZone.getRawOffset() ;
	}
	
	public void getCurrentDayZeroMillis() throws ParseException{
		long t1 = System.currentTimeMillis() ;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String format = sdf.format(new Date()) ;
		long time = sdf.parse(format).getTime() ;
		
		long t2 = System.currentTimeMillis() ;
		System.out.println("T2-T1 =" + (t2 -t1));
		System.out.println(format);
		System.out.println( time) ;
		long t3 = System.currentTimeMillis() ;
		Calendar calendar = Calendar.getInstance() ;
		calendar.set(Calendar.HOUR_OF_DAY, 0) ;
		calendar.set(Calendar.MINUTE, 0) ;
		calendar.set(Calendar.SECOND, 0) ;
		calendar.set(Calendar.MILLISECOND, 0) ;
		long calendarLong = calendar.getTimeInMillis() ;
		long t4 = System.currentTimeMillis() ;
		System.out.println("T4-T3 =" + (t4 -t3));
		System.out.println(calendarLong);
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(sdf2.format(new Date(calendarLong))) ;
	}
	
	public void getDayEndMillis(){
		long t3 = System.currentTimeMillis() ;
		Calendar calendar = Calendar.getInstance() ;
		calendar.set(Calendar.HOUR_OF_DAY, 24) ;
		calendar.set(Calendar.MINUTE, 0) ;
		calendar.set(Calendar.SECOND, 0) ;
		calendar.set(Calendar.MILLISECOND, 0) ;
		long calendarLong = calendar.getTimeInMillis() ;
		long t4 = System.currentTimeMillis() ;
		System.out.println("T4-T3 =" + (t4 -t3));
		System.out.println(calendarLong);
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(sdf2.format(new Date(calendarLong))) ;
	}
}
