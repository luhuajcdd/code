package com.java.net;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class RealmNameAnalysis {
	
	public InetAddress getServerIP(String realmName) {
		InetAddress myServer = null ;
	        try {
	        	myServer = InetAddress.getByName(realmName);
	        } catch (UnknownHostException e) {
	        	
	        }
	        return (myServer);
	 }
	
	public static void main(String[] args) {
		InetAddress inetAddress = new RealmNameAnalysis().getServerIP("rokerychn.eicp.net") ;
		System.out.println(inetAddress.getHostAddress());
		System.out.println(inetAddress.getHostName());
		inetAddress = new RealmNameAnalysis().getServerIP("192.168.1.12") ;
		System.out.println(inetAddress.getHostAddress());
	}
	
}
