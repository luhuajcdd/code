/*
 *  Copyright 2012 by Handsomedylan
 *	使用AlertDialog的思路进行了封装喵,同时以合适的字体自适应各种屏幕。
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package com.android.test.widget;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.android.test.R;

public class DatePickWheelDialog extends Dialog {

	private static int START_YEAR = 1990, END_YEAR = 2100;
	private WheelView wv_year;
	private WheelView wv_month;
	private WheelView wv_day;
	private WheelView wv_hours;
	private WheelView wv_mins;
	String[] months_big = { "1", "3", "5", "7", "8", "10", "12" };
	String[] months_little = { "4", "6", "9", "11" };

	final List<String> mList_big = Arrays.asList(months_big);
	final List<String> mList_little = Arrays.asList(months_little);
	private final Activity mActivity;
	private Button mBtn_sure;
	private TextView mTitle ;
	private CharSequence mPositiveText;
	private Button mBtn_cancel;
	private CharSequence mNegativeText;
	private Calendar mCalendar;
	private View.OnClickListener mPositiveClickListener;
	private View.OnClickListener mNegativeClickListener;

	private DatePickWheelDialog(Activity activity) {
		super(activity,R.style.theme_customer_progress_dialog);
		
		this.mActivity = activity;
		
		Window window = getWindow();
		//设置显示动画
		window.setWindowAnimations(R.style.main_menu_animstyle);
		WindowManager.LayoutParams wl = window.getAttributes();
		wl.x = 0;
		wl.y = ((Activity)activity).getWindowManager().getDefaultDisplay().getHeight();
		//设置显示位置
		onWindowAttributesChanged(wl);
		//设置点击外围解散
		setCanceledOnTouchOutside(true);
	}

	private DatePickWheelDialog(Activity activity, Calendar instance) {
		
		this(activity);
		mCalendar = instance;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		this.setContentView(R.layout.layout_datetime);

		findView();
		adjustView();
		setListener();
		setDate(mCalendar);

	}

	private void adjustView() {
		
		// 根据屏幕密度来指定选择器字体的大小
		int textSize = 0;

		textSize = pixelsToDip(mActivity.getResources(), 13);

		wv_day.TEXT_SIZE = textSize;
		wv_hours.TEXT_SIZE = textSize;
		wv_mins.TEXT_SIZE = textSize;
		wv_month.TEXT_SIZE = textSize;
		wv_year.TEXT_SIZE = textSize;
	}

	public static int pixelsToDip(Resources res, int pixels) {
		final float scale = res.getDisplayMetrics().density;
		return (int) (pixels * scale + 0.5f);
	}

	private void setListener() {
		
		wv_year.addChangingListener(wheelListener_year);
		wv_month.addChangingListener(wheelListener_month);
		wv_day.addChangingListener(wheelListener_day) ;
		wv_hours.addChangingListener(wheelListener_hours) ;
		wv_mins.addChangingListener(wheelListener_mins) ;
		// 取消
		if (mNegativeClickListener != null) {
			mBtn_cancel.setOnClickListener(mNegativeClickListener);
		} else {
			mBtn_cancel.setOnClickListener(dismissListener);
		}
		if (mPositiveClickListener != null) {
			mBtn_sure.setOnClickListener(mPositiveClickListener);
		} else {
			mBtn_sure.setOnClickListener(dismissListener);
		}

	}

	private final View.OnClickListener dismissListener = new View.OnClickListener() {

		@Override
		public void onClick(View v) {
			dismiss();
		}
	};

	private void findView() {
		
		// 年
		wv_year = (WheelView) findViewById(R.id.year);
		wv_year.setAdapter(new NumericWheelAdapter(START_YEAR, END_YEAR));// 设置"年"的显示数据
		wv_year.setLabel("年");// 添加文字

		// 月
		wv_month = (WheelView) findViewById(R.id.month);
		wv_month.setAdapter(new NumericWheelAdapter(1, 12));
		wv_month.setLabel("月");

		// 日
		wv_day = (WheelView) findViewById(R.id.day);
		// 判断大小月及是否闰年,用来确定"日"的数据
		wv_day.setLabel("日");

		// 时
		wv_hours = (WheelView) findViewById(R.id.hour);
		wv_hours.setAdapter(new NumericWheelAdapter(0, 23));
		wv_hours.setLabel("时");

		// 分
		wv_mins = (WheelView) findViewById(R.id.mins);
		wv_mins.setAdapter(new NumericWheelAdapter(0, 59, "%02d"));
		wv_mins.setLabel("分");

		mBtn_sure = (Button) findViewById(R.id.btn_datetime_sure);
		if (mPositiveText != null) {
			mBtn_sure.setVisibility(View.VISIBLE);
			mBtn_sure.setText(mPositiveText);
		}
		mBtn_cancel = (Button) findViewById(R.id.btn_datetime_cancel);
		if (mNegativeText != null) {
			mBtn_cancel.setVisibility(View.VISIBLE);
			mBtn_cancel.setText(mNegativeText);
		}
		
		mTitle = (TextView)findViewById(R.id.date_title) ;
		mTitle.setText("") ;
		
	}

	// 添加"年"监听
	private final OnWheelChangedListener wheelListener_year = new OnWheelChangedListener() {
		@Override
		public void onChanged(WheelView wheel, int oldValue, int newValue) {
			
			//检查时间
			checkTimeEnable() ;
			
			int year_num = newValue + START_YEAR;
			// 判断大小月及是否闰年,用来确定"日"的数据
			if (mList_big
					.contains(String.valueOf(wv_month.getCurrentItem() + 1))) {
				wv_day.setAdapter(new NumericWheelAdapter(1, 31));
			} else if (mList_little.contains(String.valueOf(wv_month
					.getCurrentItem() + 1))) {
				wv_day.setAdapter(new NumericWheelAdapter(1, 30));
			} else {
				if ((year_num % 4 == 0 && year_num % 100 != 0)
						|| year_num % 400 == 0)
					wv_day.setAdapter(new NumericWheelAdapter(1, 29));
				else
					wv_day.setAdapter(new NumericWheelAdapter(1, 28));
			}
		}

	};
	
	// 添加"月"监听
	private final OnWheelChangedListener wheelListener_month = new OnWheelChangedListener() {
		@Override
		public void onChanged(WheelView wheel, int oldValue, int newValue) {
			
			//检查时间
			checkTimeEnable() ;
			
			int month_num = newValue + 1;
			// 判断大小月及是否闰年,用来确定"日"的数据
			if (mList_big.contains(String.valueOf(month_num))) {
				wv_day.setAdapter(new NumericWheelAdapter(1, 31));
			} else if (mList_little.contains(String.valueOf(month_num))) {
				wv_day.setAdapter(new NumericWheelAdapter(1, 30));
			} else {
				if (((wv_year.getCurrentItem() + START_YEAR) % 4 == 0 && (wv_year
						.getCurrentItem() + START_YEAR) % 100 != 0)
						|| (wv_year.getCurrentItem() + START_YEAR) % 400 == 0)
					wv_day.setAdapter(new NumericWheelAdapter(1, 29));
				else
					wv_day.setAdapter(new NumericWheelAdapter(1, 28));
			}
		}
	};

	//  添加"日"监听
	private final OnWheelChangedListener wheelListener_day = new OnWheelChangedListener() {

		@Override
		public void onChanged(WheelView wheel, int oldValue, int newValue) {
			//检查时间
			checkTimeEnable() ;
		}
		
	};
	
	//  添加"时"监听
	private final OnWheelChangedListener wheelListener_hours = new OnWheelChangedListener() {
		
		@Override
		public void onChanged(WheelView wheel, int oldValue, int newValue) {
			//检查时间
			checkTimeEnable() ;
		}
		
	};
	
	//  添加"分"监听
	private final OnWheelChangedListener wheelListener_mins = new OnWheelChangedListener() {
		
		@Override
		public void onChanged(WheelView wheel, int oldValue, int newValue) {
			//检查时间
			checkTimeEnable() ;
		}
		
	};
	
	private void checkTimeEnable() {
		Calendar currentCalendar = Calendar.getInstance() ;
		Calendar setCalendar = getSetCalendar() ;
		Calendar initCalendar = mCalendar ;
		initCalendar.set(Calendar.SECOND, 0) ;
		int result = setCalendar.compareTo(initCalendar) ;
		if(result >= 0){
			if(setCalendar.compareTo(currentCalendar) >= 0){
				mBtn_sure.setEnabled(true) ;
			}
		}else{
			mBtn_sure.setEnabled(false) ;
		}
		
		setTitle(setCalendar);
	}

	private void setTitle(Calendar setCalendar) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm",Locale.CHINA) ;
		mTitle.setText(format.format(setCalendar.getTime())) ;
	}
	
	private void setPositiveButton(CharSequence mPositiveButtonText,
			View.OnClickListener onClickListener) {
		mPositiveText = mPositiveButtonText;
		mPositiveClickListener = onClickListener;// can't use btn_sure here
												// because it's on defined yet
	}

	private void setNegativeButton(CharSequence mNegativeButtonText,
			View.OnClickListener onClickListener) {
		mNegativeText = mNegativeButtonText;
		mNegativeClickListener = onClickListener;// can't use btn_sure here
												// because it's on defined yet
	}

	private void setCalendar(Calendar calendar) {
		
		this.mCalendar = calendar;
	}

	public DatePickWheelDialog setDate(Calendar calendar) {
		if (calendar == null)
			return this;
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DATE);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);

		wv_year.setCurrentItem(year - START_YEAR);// 初始化时显示的数据
		wv_month.setCurrentItem(month);
		if (mList_big.contains(String.valueOf(month + 1))) {
			wv_day.setAdapter(new NumericWheelAdapter(1, 31));
		} else if (mList_little.contains(String.valueOf(month + 1))) {
			wv_day.setAdapter(new NumericWheelAdapter(1, 30));
		} else {
			// 闰年
			if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
				wv_day.setAdapter(new NumericWheelAdapter(1, 29));
			else
				wv_day.setAdapter(new NumericWheelAdapter(1, 28));
		}
		wv_day.setCurrentItem(day-1);
		wv_hours.setCurrentItem(hour);
		wv_mins.setCurrentItem(minute);
		setTitle(calendar) ;
		return this;
	}

	public Calendar getSetCalendar() {
		
		Calendar c = Calendar.getInstance();
		c.set(wv_year.getCurrentItem() + START_YEAR, wv_month.getCurrentItem(),
				wv_day.getCurrentItem() + 1, wv_hours.getCurrentItem(),
				wv_mins.getCurrentItem());
		return c;
	}
	
	public Calendar getSetCalendarNoSecond() {
		
		Calendar c = Calendar.getInstance();
		c.set(wv_year.getCurrentItem() + START_YEAR, wv_month.getCurrentItem(),
				wv_day.getCurrentItem() + 1, wv_hours.getCurrentItem(),
				wv_mins.getCurrentItem());
		c.set(Calendar.SECOND, 0) ;
		return c;
	}

	public static class Builder {
		private final DatePickParams P;

		public Builder(Activity activity) {
			P = new DatePickParams(activity);
		}
		
		public Builder(Activity activity, Calendar c) {
			P = new DatePickParams(activity,c);
		}

		public Builder setTitle(CharSequence title) {
			P.mTitle = title;
			return this;
		}

		public Builder setIcon(int iconId) {
			P.mIconId = iconId;
			return this;
		}

		public Builder setPositiveButton(CharSequence text,
				final View.OnClickListener listener) {
			P.mPositiveButtonText = text;
			P.mPositiveButtonListener = listener;
			return this;
		}

		public Builder setNegativeButton(CharSequence text,
				final View.OnClickListener listener) {
			
			P.mNegativeButtonText = text;
			P.mNegativeButtonListener = listener;
			return this;
		}

		public DatePickWheelDialog create() {
			final DatePickWheelDialog dialog = new DatePickWheelDialog(
					P.mActivity);
			P.apply(dialog);
			return dialog;
		}
	}

	public static class DatePickParams {
		public int mIconId;
		public View.OnClickListener mPositiveButtonListener;
		public CharSequence mPositiveButtonText;
		public CharSequence mTitle;
		public final Activity mActivity;
		public Calendar calendar;
		private CharSequence mNegativeButtonText;
		private View.OnClickListener mNegativeButtonListener;

		public DatePickParams(Activity activity) {
			mActivity = activity;
			calendar = Calendar.getInstance();
		};

		public DatePickParams(Activity activity, Calendar calendar) {
			mActivity = activity;
			this.calendar = calendar;
		}

		public void apply(DatePickWheelDialog dialog) {
			if (mTitle != null) {
				dialog.setTitle(mTitle);
			}

			if (mPositiveButtonText != null) {
				dialog.setPositiveButton(mPositiveButtonText,
						mPositiveButtonListener);
			}
			if (mNegativeButtonText != null) {
				dialog.setNegativeButton(mNegativeButtonText,
						mNegativeButtonListener);
			}
			if (calendar != null)
				dialog.setCalendar(calendar);

		}
	}

}
