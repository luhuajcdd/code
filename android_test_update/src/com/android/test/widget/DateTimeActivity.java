package com.android.test.widget;

import java.text.DecimalFormat;
import java.util.Calendar;

import com.android.test.R;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class DateTimeActivity extends Activity {
	private Button btn;
	private DatePickWheelDialog datePickWheelDialog;
	private TextView tv;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_datetime_main);
		btn = (Button) findViewById(R.id.button1);
		tv = (TextView) findViewById(R.id.text1);
		btn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Calendar c = Calendar.getInstance() ;
				c.add(Calendar.MINUTE, 5) ;
				datePickWheelDialog = new DatePickWheelDialog.Builder(DateTimeActivity.this , c)
						.setPositiveButton("确定", new View.OnClickListener() {

							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								Calendar c = datePickWheelDialog
										.getSetCalendar();
								tv.setText(getFormatTime(c));
								datePickWheelDialog.dismiss();
							}
						})
						.setNegativeButton("取消", null)
						.create();
				datePickWheelDialog.show();
			}
		});
		
	}

	public static String getFormatTime(Calendar c) {
		String parten = "00";
		DecimalFormat decimal = new DecimalFormat(parten);
		// 设置日期的显示
		Calendar calendar = c;
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int day = calendar.get(Calendar.DATE);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		return year + "-" + decimal.format(month + 1) + "-"
				+ decimal.format(day) + " " + decimal.format(hour) + ":"
				+ decimal.format(minute);

	}
}
