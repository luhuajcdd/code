package com.android.test.loader;

import java.util.List;
import java.util.Map;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.app.LoaderManager.LoaderCallbacks;
import android.support.v4.content.Loader;
import android.util.Log;

import com.android.test.R;
import com.android.test.loader.HttpAsyncLoader.HttpType;
import com.android.test.loader.LoaderURLConstants.QueryType;
import com.android.test.loader.param.WorkFlowData.DataItem;

public class JsonLoaderActivity extends FragmentActivity implements LoaderCallbacks<Map<QueryType, List<DataItem>>>{
	
	private String TAG = JsonLoaderActivity.class.getSimpleName() ;
	
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.layout_json_loader) ;
		LoaderManager loaderManager =  getSupportLoaderManager() ;
		loaderManager.initLoader(0, null,  this);
	}
	
	@Override
	public Loader<Map<QueryType, List<DataItem>>> onCreateLoader(int id, Bundle args) {
		Log.i(TAG, "id = " + id + ", args = " + args) ;
		HttpAsyncLoader.Builder builder = new HttpAsyncLoader.Builder(this) ;
		builder.setParams(LoaderURLConstants.RequestParam.METHOD, "get") ;
		builder.setParams(LoaderURLConstants.RequestParam.QUERY_TYPE, 
				""+QueryType.WILL_HANDLE.ordinal()+","+QueryType.HANDLE_OVER.ordinal()) ;
		builder.setParams(LoaderURLConstants.RequestParam.ACTOR_USER, "1") ;
		//builder.setHeader(key, value) ;
		builder.setUrl(LoaderURLConstants.MoaURL.GET_FLOWS) ;
		builder.setHttpType(HttpType.GET) ;
		return builder.loader() ;
	}
	
	@Override
	public void onLoadFinished(Loader<Map<QueryType, List<DataItem>>> loader, Map<QueryType, List<DataItem>> data) {
		Log.i(TAG, "loader = " + loader + ", data = " + data) ;
	}
	
	@Override
	public void onLoaderReset(Loader<Map<QueryType, List<DataItem>>> loader) {
		
	}
	
}
