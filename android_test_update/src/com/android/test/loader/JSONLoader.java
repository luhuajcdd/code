package com.android.test.loader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.impl.cookie.BasicClientCookie;
import org.json.JSONObject;

import android.content.AsyncTaskLoader;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

public class JSONLoader extends AsyncTaskLoader<JSONObject> {

	private String TAG = JSONLoader.class.getSimpleName() ;
	
	private boolean mLoaded;
	private JSONObject mData;
	private Parameters mParam;

	// 处理cookie部分
	private static List<BasicClientCookie> cookieList = new ArrayList<BasicClientCookie>();

	public void addCookie(BasicClientCookie cookie) {
		cookieList.add(cookie);
	}

	public void delCookie(BasicClientCookie cookie) {
		cookieList.remove(cookie);
	}

	public List<BasicClientCookie> getAllCookie() {
		return cookieList;
	}

	public String getCookie(String src, String name) {
		if (null == src || null == name) {
			return null;
		}

		URL url = null;
		try {
			url = new URL(src);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return null;
		}

		String domain = url.getHost().toLowerCase();

		String cookieString = null;
		for (BasicClientCookie cookie : cookieList) {
			if (cookie.getDomain().equals(domain)
					&& cookie.getName().equals(name)) {
				cookieString = cookie.getValue();
				break;
			}
		}

		return cookieString;
	}

	public void setCookie(String src, String name, String value) {
		if (null == src || null == name) {
			return;
		}

		BasicClientCookie cookie = null;
		URL url = null;
		try {
			url = new URL(src);
		} catch (MalformedURLException e) {
			e.printStackTrace();
			return;
		}
		for (BasicClientCookie tmpCookie : cookieList) {
			if (tmpCookie.getName().equals(name)
					&& tmpCookie.getDomain()
							.equals(url.getHost().toLowerCase())) {
				tmpCookie.setValue(value);
				cookie = tmpCookie;
				break;
			}
		}
		if (null == cookie) {
			cookie = new BasicClientCookie(name, value);
			cookie.setDomain(url.getHost().toLowerCase());
			addCookie(cookie);
		}
	}

	public static void initializeCookies() {
		cookieList.clear();
	}

	public static void setBasicCookie(String url,String name,String value) {
		BasicClientCookie cookie = null;
		for (BasicClientCookie tmpCookie : cookieList) {
			if (tmpCookie.getName().equals(name)) {
				tmpCookie.setValue(value);
				cookie = tmpCookie;
				break;
			}
		}
		if (null == cookie) {
			cookie = new BasicClientCookie(name, value);
			try {
				cookie.setDomain(new URL(url).getHost().toLowerCase());
			} catch (MalformedURLException e) {
				return;
			}
			cookieList.add(cookie);
		}
	}

	private void applyCookieHeader(HttpURLConnection httpConn) {
		String domain = httpConn.getURL().getHost().toLowerCase();
		StringBuilder sb = new StringBuilder();

		for (BasicClientCookie cookie : cookieList) {
			if (cookie.getDomain().equals(domain)) {
				if (sb.length() > 0) {
					sb.append(";");
				}
				sb.append(cookie.getName());
				sb.append("=");
				sb.append(cookie.getValue());
			}
		}

		if (sb.length() > 0) {
			httpConn.addRequestProperty(TAG,
					sb.toString());
			;
		}
	}

	private void readResponseHeader(HttpURLConnection httpConn) {
		if (null == httpConn) {
			return;
		}
		Map<String, List<String>> fields = httpConn.getHeaderFields();
		List<String> cookies = fields.get(TAG);
		String name = null;
		String value = null;

		if (null == cookies) {
			return;
		}
		for (String cookieString : cookies) {
			String cookieParam = cookieString.split(";")[0];
			String[] simgleCookie = cookieParam.split("=");
			if (simgleCookie.length == 2) {
				name = simgleCookie[0];
				value = simgleCookie[1];
				BasicClientCookie cookie = null;
				boolean isFind = false;

				for (BasicClientCookie tmpCookie : cookieList) {
					if (tmpCookie.getName().equals(name)
							&& tmpCookie.getDomain().equals(
									httpConn.getURL().getHost().toLowerCase())) {
						if (value.toLowerCase().equals("deleted")) {
							delCookie(tmpCookie);

							isFind = true;
							break;
						}
						tmpCookie.setValue(value);
						cookie = tmpCookie;
						isFind = true;
						break;
					}
				}
				if (!isFind) {
					cookie = new BasicClientCookie(name, value);
					cookie.setDomain(httpConn.getURL().getHost().toLowerCase());
					addCookie(cookie);
				}
			}
		}
	}

	public static class Builder {
		private Context mContext;
		private Parameters mParam = new Parameters();

		public Builder(Context context) {
			mContext = context;
		}

		public Builder setUrl(String url) {
			mParam.url = url;
			return this;
		}

		public Builder setRequestMethod(String method) {
			mParam.requestMethod = method;
			return this;
		}

		public Builder setCookiesEnabled(boolean enabled) {
			mParam.enableCookies = enabled;
			return this;
		}

		public Builder putArgument(String key, Object value) {
			if (mParam.arguments == null) {
				mParam.arguments = new HashMap<String, String>();
			}
			mParam.arguments.put(key, value.toString());
			return this;
		}

		
		public Builder putArguments(Map<? extends String, ?> map) {
			if (mParam.arguments == null) {
				mParam.arguments = new HashMap<String, String>(map.size());
			}

			for (Entry<? extends String, ?> e : map.entrySet()) {
				mParam.arguments.put(e.getKey(), e.getValue().toString());
			}

			return this;
		}

		public Builder putArguments(Bundle bundle) {
			if (mParam.arguments == null) {
				mParam.arguments = new HashMap<String, String>(bundle.size());
			}

			for (String key : bundle.keySet()) {
				mParam.arguments.put(key, bundle.get(key).toString());
			}

			return this;
		}

		public Builder putHeader(String key, Object value) {
			if (mParam.headers == null) {
				mParam.headers = new HashMap<String, String>();
			}
			mParam.headers.put(key, value.toString());
			return this;
		}

		public Builder putHeaders(Map<? extends String, ?> map) {
			if (mParam.headers == null) {
				mParam.headers = new HashMap<String, String>(map.size());
			}

			for (Entry<? extends String, ?> e : map.entrySet()) {
				mParam.headers.put(e.getKey(), e.getValue().toString());
			}

			return this;
		}

		public Builder putHeaders(Bundle bundle) {
			if (mParam.headers == null) {
				mParam.headers = new HashMap<String, String>(bundle.size());
			}

			for (String key : bundle.keySet()) {
				mParam.headers.put(key, bundle.get(key).toString());
			}

			return this;
		}

		public JSONLoader create() {
			return new JSONLoader(mContext, mParam);
		}
	}

	private static class Parameters {
		String url;
		String requestMethod = "GET";
		Map<String, String> arguments;
		Map<String, String> headers;
		boolean enableCookies = true;
	}

	JSONLoader(Context context, Parameters param) {
		super(context);

		mLoaded = false;
		mParam = param;
	}

	@Override
	public JSONObject loadInBackground() {
		String url = mParam.url;
		StringBuilder sb = new StringBuilder(2048);
		if (mParam.arguments != null) {
			for (Entry<String, String> e : mParam.arguments.entrySet()) {
				if (sb.length() != 0) {
					sb.append("&");
				}
				sb.append(e.getKey()).append('=')
						.append(Uri.encode(e.getValue()));
			}
		}
		if (mParam.requestMethod.equalsIgnoreCase("GET")
				&& mParam.arguments != null) {
			url = url + "?" + sb.toString();
		}

		InputStreamReader isr = null;
		HttpURLConnection conn = null;
		Log.d(TAG, "url:" + url);
		try {
			conn = (HttpURLConnection) new URL(url).openConnection();

			if (mParam.headers != null) {
				for (Entry<String, String> e : mParam.headers.entrySet()) {
					conn.setRequestProperty(e.getKey(), e.getValue());
				}
			}
			applyCookieHeader(conn);
			if (mParam.requestMethod.equalsIgnoreCase("POST")
					&& mParam.arguments != null) {
				Log.d(TAG, "Method:POST");
				conn.setRequestMethod("POST");
				conn.setDoOutput(true);
			}
			conn.connect();
			if (mParam.requestMethod.equalsIgnoreCase("POST")
					&& mParam.arguments != null) {
				Log.d(TAG, "Write Post data");
				OutputStream outStream = null;
				outStream = conn.getOutputStream();
				outStream.write(sb.toString().getBytes("utf-8"));
				outStream.flush();
			}
			// 处理返回值
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				conn.disconnect();
				Log.d(TAG,
						String.format("fail to connect service:%d.",
								conn.getResponseCode()));
				return null;
			}
			readResponseHeader(conn);

			isr = new InputStreamReader(conn.getInputStream());
			char[] buffer = new char[512];
			int ret;
			sb.setLength(0);
			while ((ret = isr.read(buffer)) != -1) {
				sb.append(buffer, 0, ret);
			}
			return new JSONObject(sb.toString());
		} catch (Exception e) {
			Log.e("JSONLoader", "Failed loading contents.", e);
		} finally {
			conn.disconnect();
			if (isr != null) {
				try {
					isr.close();
				} catch (IOException e) {
				}
			}
		}

		return null;
	}

	@Override
	public void deliverResult(JSONObject result) {
		mLoaded = true;
		mData = result;
		if (isStarted()) {
			super.deliverResult(result);
		}
	}

	@Override
	protected void onStartLoading() {
		if (mLoaded) {
			deliverResult(mData);
		} else {
			forceLoad();
		}
	}
}
