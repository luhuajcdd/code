package com.android.test.loader;

import com.android.test.loader.LoaderURLConstants.QueryType;

public class LoaderURLConstants {
	
	public static class MoaURL{
		//IP and port 
		public final static String IP_PORT = "200.200.107.39:8080" ;
		//获取流程列表数据
		public final static String GET_FLOWS = "http://"+IP_PORT+"/SangforWorkflow/srv.action" ; 
		// 流程处理节点表单数据加载
		public final static String GET_NODE = "http://"+IP_PORT+"/SangforWorkflow/srv.action?method=loadTransition" ;
		// 流程处理审批记录表单加载
		public final static String GET_HOSITORY = "http://"+IP_PORT+"/SangforWorkflow/srv.action?method=loadComments" ;
		// 流程处理业务表单数据加载
		public final static String GET_BUSSNESS = "http://"+IP_PORT+"/SangforWorkflow/srv.action?method=loadBeanInfo" ;
	}
	
	public static class RequestParam{
		
		public final static String METHOD = "method" ;
		
		//获取流程列表数据
		//queryType	是	char(1)	查询类型，值包括：0、1、2、3，0：待处理 1：已处理 2：已结束3：我发起的(不管是否已结束)
		//actorUser	submitUser、actorUser至少传一个	char(200)	流程角色用户，（查用户待办时为当前处理用户，已办则是参与过的用户）
		//submitUser	submitUser、actorUser至少传一个	char(200)	流程提交人
		//startTime	否	char(8)	查询起始时间,文本格式：yyyyMMdd
		//endTime	否	char(8)	查询结束时间,文本格式：yyyyMMdd
		//firstRecord	否	Int(10)	查询起始记录（第多少条记录开始获取），正整数或0，取值范围：0 >= x <= 2147483647
		//maxRecord	否	Int(10)	查询记录数阀值（一次最多获取多少条），正整数或0，取值范围：0 >= x <= 2147483647
		public final static String QUERY_TYPE 	= "queryType" ;
		public final static String ACTOR_USER 	= "actorUser" ;
		public final static String SUBMIT_USER 	= "submitUser" ;
		public final static String START_TIME 	= "startTime" ;
		public final static String END_TIME 		= "endTime" ;
		public final static String FIRST_RECORD 	= "firstRecord" ;
		public final static String MAX_RECORD 	= "maxRecord" ;
		
	}
	
	public static class ResponseParam{
		//获取流程列表数据
		//submitUserName	是	char(200)	流程提交人，发起流程用户
		//submitTime	是	char(200)	提交时间,格式yyyyMMddHHmmss
		//info	是	char(500)	流程备注信息（可能值为空）
		//type	是	char(8)	类别(事假，加班申请，年休假，产假,......)
		//processInstanceId	是	bigint(18)	流程实例ID，后续查看UI时需用到
		//taskInstanceId	是	bigint(18)	节点实例ID，后续查看UI时需用到
		//currentState	是	char(300)	当前流程处理状态（流程已结束，xxx处理中）等等.....
		//currentUsers	是	char(2000)	当前处理用户（目前我们只设计为一个流程处理节点一个用户\角色处理，假设有多个用户\角色则会以“,”英文逗号分隔）
		public final static String SUBMIT_USER_NAME	= "submitUserName" ;
		public final static String SUBMIT_TIME 			= "submitTime" ;
		public final static String INFO 					= "info" ;
		public final static String TYPE 					= "type" ;
		public final static String PROCESS_INSTANCEID 	= "processInstanceId" ;
		public final static String TASK_INSTANCEID 		= "taskInstanceId" ;
		public final static String CURRENT_STATE		= "currentState" ;
		public final static String CURRENT_USERS 		= "currentUsers" ;
	}
	
	public enum QueryType{
		//0、1、2、3，0：待处理 1：已处理 2：已结束3：我发起的(不管是否已结束)
		WILL_HANDLE,
		HANDLED,
		HANDLE_OVER,
		MINE;

		
		public static QueryType getFromOrder(Integer valueOf) {
			if(valueOf == 0){
				return QueryType.WILL_HANDLE ;
			}else if(valueOf == 1){
				return QueryType.HANDLED ;
			}else if(valueOf == 2){
				return QueryType.HANDLE_OVER ;
			}else if(valueOf == 3){
				return QueryType.MINE ;
			}
			return null ;
		}
	}
	
}
