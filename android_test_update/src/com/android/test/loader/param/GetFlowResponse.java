package com.android.test.loader.param;

public class GetFlowResponse {
	// 获取流程列表数据
	// submitUserName 是 char(200) 流程提交人，发起流程用户
	// submitTime 是 char(200) 提交时间,格式yyyyMMddHHmmss
	// info 是 char(500) 流程备注信息（可能值为空）
	// type 是 char(8) 类别(事假，加班申请，年休假，产假,......)
	// processInstanceId 是 bigint(18) 流程实例ID，后续查看UI时需用到
	// taskInstanceId 是 bigint(18) 节点实例ID，后续查看UI时需用到
	// currentState 是 char(300) 当前流程处理状态（流程已结束，xxx处理中）等等.....
	// currentUsers 是 char(2000)
	// 当前处理用户（目前我们只设计为一个流程处理节点一个用户\角色处理，假设有多个用户\角色则会以“,”英文逗号分隔）
	public String	submitUserName;
	public String	submitTime;
	public String	info;
	public String	type;
	public String	processInstanceId;
	public String	taskInstanceId;
	public String	currentState;
	public String	currentUsers;
	
	public String getSubmitUserName() {
		return submitUserName;
	}
	
	public void setSubmitUserName(String submitUserName) {
		this.submitUserName = submitUserName;
	}
	
	public String getSubmitTime() {
		return submitTime;
	}
	
	public void setSubmitTime(String submitTime) {
		this.submitTime = submitTime;
	}
	
	public String getInfo() {
		return info;
	}
	
	public void setInfo(String info) {
		this.info = info;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	
	public String getTaskInstanceId() {
		return taskInstanceId;
	}
	
	public void setTaskInstanceId(String taskInstanceId) {
		this.taskInstanceId = taskInstanceId;
	}
	
	public String getCurrentState() {
		return currentState;
	}
	
	public void setCurrentState(String currentState) {
		this.currentState = currentState;
	}
	
	public String getCurrentUsers() {
		return currentUsers;
	}
	
	public void setCurrentUsers(String currentUsers) {
		this.currentUsers = currentUsers;
	}
	
}
