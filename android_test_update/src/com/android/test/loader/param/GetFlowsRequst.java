package com.android.test.loader.param;

public class GetFlowsRequst {
	//获取流程列表数据
	//queryType	是	char(1)	查询类型，值包括：0、1、2、3，0：待处理 1：已处理 2：已结束3：我发起的(不管是否已结束)
	//actorUser	submitUser、actorUser至少传一个	char(200)	流程角色用户，（查用户待办时为当前处理用户，已办则是参与过的用户）
	//submitUser	submitUser、actorUser至少传一个	char(200)	流程提交人
	//startTime	否	char(8)	查询起始时间,文本格式：yyyyMMdd
	//endTime	否	char(8)	查询结束时间,文本格式：yyyyMMdd
	//firstRecord	否	Int(10)	查询起始记录（第多少条记录开始获取），正整数或0，取值范围：0 >= x <= 2147483647
	//maxRecord	否	Int(10)	查询记录数阀值（一次最多获取多少条），正整数或0，取值范围：0 >= x <= 2147483647
	public String	queryType;
	public String	actorUser;
	public String	submitUser;
	public String	startTime;
	public String	endTime;
	public String	firstRecord;
	public String	maxRecord;
	
	public String getQueryType() {
		return queryType;
	}
	
	public void setQueryType(String queryType) {
		this.queryType = queryType;
	}
	
	public String getActorUser() {
		return actorUser;
	}
	
	public void setActorUser(String actorUser) {
		this.actorUser = actorUser;
	}
	
	public String getSubmitUser() {
		return submitUser;
	}
	
	public void setSubmitUser(String submitUser) {
		this.submitUser = submitUser;
	}
	
	public String getStartTime() {
		return startTime;
	}
	
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
	public String getEndTime() {
		return endTime;
	}
	
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	public String getFirstRecord() {
		return firstRecord;
	}
	
	public void setFirstRecord(String firstRecord) {
		this.firstRecord = firstRecord;
	}
	
	public String getMaxRecord() {
		return maxRecord;
	}
	
	public void setMaxRecord(String maxRecord) {
		this.maxRecord = maxRecord;
	}
	
}
