package com.android.test.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.util.Log;

import com.android.test.sqlite.db.User;
import com.google.gson.Gson;

public class AsyncTaskLoaderExample extends AsyncTaskLoader<String> {

	private String TAG = AsyncTaskLoaderExample.class.getSimpleName() ;
	private String mData ;
	
	public AsyncTaskLoaderExample(Context context) {
		super(context);
		Log.i(TAG, "AsyncTaskLoaderExample()") ;
	}


	@Override
	protected void onStartLoading() {
		Log.i(TAG, "onStartLoading()") ;
		if(!TextUtils.isEmpty(mData)){
			super.deliverResult(mData) ;
		}else {
			forceLoad();
		}
	}

	@Override
	protected void onStopLoading() {
		Log.i(TAG, "onStopLoading()") ;
		super.onStopLoading();
		
	}
	
	
	@Override
	public void forceLoad() {
		Log.i(TAG, "forceLoad()") ;
		super.forceLoad();
		
	}
	
	@Override
	protected void onForceLoad() {
		Log.i(TAG, "onForceLoad()") ;
		super.onForceLoad();
		
	}
	
	@Override
	public String loadInBackground() {
		Log.i(TAG, "loadInBackground()") ;
		User user = new User() ;
		user.id = 1 ;
		user.age = 2 ;
		user.name = "1a" ;
		Gson gson = new Gson();
		return gson.toJson(user) ;
	}

	
	@Override
	public void deliverResult(String data) {
	
		mData = data ;
		Log.i(TAG, "deliverResult() data =  " + data  ) ;
		super.deliverResult(data);
	}
	
	@Override
	public boolean cancelLoad() {
		Log.i(TAG, "cancelLoad()") ;
		return super.cancelLoad();
	}
	
}
