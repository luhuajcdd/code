package com.android.test.loader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpParams;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.text.TextUtils;
import android.util.Log;

import com.android.test.loader.LoaderURLConstants.QueryType;
import com.android.test.loader.param.WorkFlowData;
import com.android.test.loader.param.WorkFlowData.DataItem;
import com.google.gson.Gson;

public class HttpAsyncLoader extends AsyncTaskLoader<Map<QueryType, List<DataItem>>> {
	private String				TAG	= HttpAsyncLoader.class.getSimpleName();
	
	private Map<QueryType, List<DataItem>>	mData;
	
	private String 				mUrl ;
	private Map<String, String> 	mHeaders = new HashMap<String , String>() ;
	private Map<String, Object> 	mParams = new HashMap<String , Object>() ;
	private HttpType 			mHttpType ;
	
	private HttpAsyncLoader(Context context) {
		super(context);
	}
	
	public enum HttpType {
		GET, POST, PUT, DELETE
	}
	
	@Override
	public Map<QueryType, List<DataItem>> loadInBackground() {
		
		if(HttpType.GET == mHttpType){
			return get();
		}else if(HttpType.POST == mHttpType){
		}else if(HttpType.PUT == mHttpType){
		}else if(HttpType.DELETE == mHttpType){
		}
		return null ;
	}

	private Map<QueryType, List<DataItem>> get() {
		HttpClient client = new DefaultHttpClient();
		String url = mUrl ;
		if(TextUtils.isEmpty(url)){
			throw new NullPointerException("Url is null") ;
		}
		url = setGetParams(url);
		List<QueryType> queryTypes = new ArrayList<LoaderURLConstants.QueryType>() ;
		if(mParams.size() > 0){
			String queryType = (String) mParams.get(LoaderURLConstants.RequestParam.QUERY_TYPE) ;
			String[] qts = queryType.split(",") ;
			for(String qt : qts){
				queryTypes.add(QueryType.getFromOrder(Integer.valueOf(qt)));
			}
		}
		HttpGet get = new HttpGet(url);
		setGetHeaders(get) ;
		HttpResponse response = null;
		try {
			response = client.execute(get);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int statusCode = response.getStatusLine().getStatusCode() ;
		if(HttpStatus.SC_OK == statusCode){
			String data = handleResponse(response) ;
			Gson gson = new Gson() ;
			WorkFlowData workFlowData = gson.fromJson(data, WorkFlowData.class) ;
			int retCode = workFlowData.retCode ;
			String retMsg = workFlowData.retMsg ;
			if(retCode == 0){
				Map<QueryType, List<DataItem>> dataMap = new HashMap<LoaderURLConstants.QueryType, List<DataItem>>() ;
				for(int i = 0 ; i < queryTypes.size() ; i ++){
					dataMap.put(queryTypes.get(i), workFlowData.datas.get(i)) ;
				}
				return dataMap ;
			}else{
				Log.e(TAG, retMsg) ;
				return null ;
			}
		}
		
		// Get the response
		return null;
	}

	private void setGetHeaders(HttpGet get) {
		if(mHeaders.size() > 0){
			for(Entry<String, String> entry : mHeaders.entrySet()){
				get.setHeader(entry.getKey(), entry.getValue()) ;
			}
		}
	}

	private String setGetParams(String url) {
		if(mParams.size() > 0){
			url += "?" ;
			for(Entry<String, Object> entry : mParams.entrySet()){
				url += entry.getKey() + "=" + entry.getValue().toString() +"&" ;
			}
			if(url.indexOf("&") == (url.length() - 1) ){
				url = url.substring(0, url.length() - 1) ;
			}
		}
		return url ;
	}
	
	@SuppressWarnings("unused")
	private void setParams(HttpClient client) {
		if(mParams.size() > 0){
			HttpParams params = client.getParams() ;
			for(Entry<String, Object> entry : mParams.entrySet()){
				params.setParameter(entry.getKey(), entry.getValue()) ;
			}
		}
	}

	private String handleResponse(HttpResponse response) {
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuilder str = new StringBuilder();
			String line = "";
			while ((line = rd.readLine()) != null) {
				str.append(line);
			}
			return str.toString();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null ;
	}
	
	@Override
	protected void onStartLoading() {
		super.onStartLoading();
		if (mData != null) {
			super.deliverResult(mData);
		} else {
			forceLoad();
		}
	}
	
	@Override
	protected void onStopLoading() {
		super.onStopLoading();
		cancelLoad();
	}
	
	@Override
	public void onCanceled(Map<QueryType, List<DataItem>> data) {
		super.onCanceled(data);
		onReleaseResources(data);
	}
	
	@Override
	public void deliverResult(Map<QueryType, List<DataItem>> data) {
		if (isReset()) {
			// An async query came in while the loader is stopped.
			// We
			// don't need the result.
			if (data != null) {
				onReleaseResources(data);
			}
		}
		
		if (isStarted()) {
			// If the Loader is currently started, we can
			// immediately
			// deliver its results.
			super.deliverResult(data);
		}
		
		if (data != null) {
			onReleaseResources(data);
		}
		
	}
	
	@Override
	protected void onReset() {
		super.onReset();
		// Ensure the loader is stopped
	        onStopLoading();
	        if (mData != null) {
	                onReleaseResources(mData);
	                mData = null;
	       }
	}
	
	private void onReleaseResources(Map<QueryType, List<DataItem>> data) {
		data.clear() ;
	}
	
	public static class Builder{
		
		private HttpAsyncLoader mHttpAsyncLoader ;
		
		public Builder(Context context){
			mHttpAsyncLoader = new HttpAsyncLoader(context) ;
		}
		
		public void setHeader(String key, String value){
			if(TextUtils.isEmpty(key)){
				throw new NullPointerException("key is null") ;
			}
			mHttpAsyncLoader.mHeaders.put(key, value) ;
		}
		
		public void setParams(String key, Object value){
			if(TextUtils.isEmpty(key)){
				throw new NullPointerException("key is null") ;
			}
			mHttpAsyncLoader.mParams.put(key, value) ;
		}
		
		public void setHttpType(HttpType type){
			mHttpAsyncLoader.mHttpType = type ;
		}
		
		public void setUrl(String url){
			mHttpAsyncLoader.mUrl = url ;
		}
		
		public HttpAsyncLoader loader(){
			return mHttpAsyncLoader ;
		}
		
	}
}
