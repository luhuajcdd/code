package com.android.test.annimation.levellist;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.test.R;

public class LevelListActivity extends Activity {

	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_animation_level_list) ;
		final	Button b = (Button)this.findViewById(R.id.button1) ;
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				System.out.println("level change"+b.getBackground().getLevel());
				
				b.getBackground().setLevel(1) ;
			}
		}) ;
	}
}
