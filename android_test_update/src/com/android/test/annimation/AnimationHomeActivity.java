package com.android.test.annimation;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.android.test.R;

public class AnimationHomeActivity extends Activity implements View.OnClickListener {
	
	private Button view_animation,  animator_objectSet , objectanimator_alpha ,
					translation, rotate, scale, pivot, colorStateListResource,
					levelList,transition;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_animation_home) ;
		view_animation = (Button)findViewById(R.id.view_animation) ;
		animator_objectSet = (Button)findViewById(R.id.animator_objectSet) ;
		objectanimator_alpha = (Button)findViewById(R.id.objectanimator_alpha) ;
		objectanimator_alpha = (Button)findViewById(R.id.objectanimator_alpha) ;
		translation = (Button)findViewById(R.id.translation) ;
		rotate = (Button)findViewById(R.id.rotate) ;
		scale = (Button)findViewById(R.id.scale) ;
		pivot = (Button)findViewById(R.id.pivot) ;
		colorStateListResource = (Button)findViewById(R.id.color_state_list_resource) ;
		transition = (Button)findViewById(R.id.transition) ;
		levelList = (Button)findViewById(R.id.level_list) ;
		
		view_animation.setOnClickListener(this) ;
		animator_objectSet.setOnClickListener(this) ;
		objectanimator_alpha.setOnClickListener(this) ;
		translation.setOnClickListener(this) ;
		rotate.setOnClickListener(this) ;
		scale.setOnClickListener(this) ;
		pivot.setOnClickListener(this) ;
		colorStateListResource.setOnClickListener(this) ;
		levelList.setOnClickListener(this) ;
		transition.setOnClickListener(this) ;
		//test();
	}
	
	void test(){
		ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
    executor.scheduleWithFixedDelay(new Runnable(){

        public void run() {
        	System.out.println("test() ------------> run()");
//update data to server

        }

    }, 0, 1, TimeUnit.SECONDS);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.view_animation:
			startActivity(new Intent("android.intent.action.view_animation")) ;
			break;
		case R.id.animator_objectSet:
			startActivity(new Intent("android.intent.action.object_animationset")) ;
			break;
		case R.id.objectanimator_alpha:
			startActivity(new Intent("android.intent.action.object_animation_alpha")) ;
			break ;
		case R.id.translation:
			startActivity(new Intent("android.intent.action.object_animation_translation")) ;
		case R.id.rotate:
			startActivity(new Intent("android.intent.action.object_animation_rotate")) ;
			break ;
		case R.id.scale:
			startActivity(new Intent("android.intent.action.object_animation_scale")) ;
			break ;
		case R.id.pivot:
			startActivity(new Intent("android.intent.action.object_animation_pivot")) ;
			break ;
		case R.id.color_state_list_resource:
			startActivity(new Intent("android.intent.action.color_state_list_resource")) ;
			break ;
		case R.id.level_list:
			startActivity(new Intent("android.intent.action.level_list")) ;
			break ;
		case R.id.transition:
			startActivity(new Intent("android.intent.action.transition")) ;
			break ;
		default:
			break;
		}
	}
	
}
