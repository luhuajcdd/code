package com.android.test.annimation.transition;

import android.app.Activity;
import android.graphics.drawable.ClipDrawable;
import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.android.test.R;

public class TransitionActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_animation_transition) ;
		ImageButton button = (ImageButton) findViewById(R.id.button1);
		TransitionDrawable drawable = (TransitionDrawable) button.getDrawable();
		drawable.startTransition(5000);
		
		ImageView imageview = (ImageView) findViewById(R.id.image);
		ClipDrawable drawable2 = (ClipDrawable) imageview.getDrawable();
		drawable2.setLevel(drawable2.getLevel() + 1000);
	}
	
}
