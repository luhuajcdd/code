package com.android.test.annimation.animator;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;

import com.android.test.R;

public class ObjectAnimatorTranslation extends Activity {
	
	private ImageView phone, nfc ; 
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_animation_animator);
		phone =(ImageView)this.findViewById(R.id.imageView2) ;
		nfc =(ImageView)this.findViewById(R.id.imageView1) ;
	
		final AnimatorSet set = new AnimatorSet() ;            
		ObjectAnimator anim = ObjectAnimator .ofFloat(phone, "translationX", -500f, 0f);
		anim.setDuration(2000);
		ObjectAnimator anim3 = ObjectAnimator .ofFloat(phone, "translationX", 0f, -500f);
		anim3.setDuration(2000);
		ObjectAnimator anim2 = ObjectAnimator .ofFloat(phone, "translationY", 0f, -500f);
		anim2.setDuration(2000);
		ObjectAnimator anim4 = ObjectAnimator .ofFloat(phone, "translationY", -500f, 0f);
		anim4.setDuration(2000);
		
		
		AnimatorSet set3 = new AnimatorSet();
		set3.play(anim4).before(anim3) ;
		AnimatorSet set2 = new AnimatorSet();
		set2.play(anim2).before(set3) ;
		set.play(anim).before(set2);
		set.start();
		set.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				// TODO Auto-generated method stub
				set.start();
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
				// TODO Auto-generated method stub
				
			}
		});
		
		//ObjectAnimator fadeOut = ObjectAnimator.ofFloat(nfc, "alpha", 0f);
	   // ObjectAnimator mover = ObjectAnimator.ofFloat(phone, "translationX", -500f, 0f);
	   // ObjectAnimator fadeIn = ObjectAnimator.ofFloat(phone, "alpha", 0f, 1f);
	   // AnimatorSet animSet = new AnimatorSet() ;
	   // animSet.play(mover)/*.with(fadeIn).after(fadeOut)*/;
	    //animSet.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}


}
