package com.android.test.annimation.animator;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;

import com.android.test.R;

public class ObjectAnimatorScale extends Activity {
	
	private ImageView phone, nfc ; 
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_animation_animator);
		phone =(ImageView)this.findViewById(R.id.imageView2) ;
		nfc =(ImageView)this.findViewById(R.id.imageView1) ;
	
		final AnimatorSet set = new AnimatorSet() ;            
		ObjectAnimator anim = ObjectAnimator .ofFloat(phone, "scaleX", 1f);
		anim.setDuration(1000);
		ObjectAnimator anim2 = ObjectAnimator .ofFloat(phone, "scaleX", 0.5f);
		anim2.setDuration(1000);
		ObjectAnimator anim3 = ObjectAnimator .ofFloat(phone, "scaleY", 1f);
		anim3.setDuration(1000);
		ObjectAnimator anim4 = ObjectAnimator .ofFloat(phone, "scaleY", 0.5f);
		anim4.setDuration(1000);
		
		ObjectAnimator anim5 = ObjectAnimator .ofFloat(phone, "scaleX", 0.5f);
		anim5.setDuration(1000);
		ObjectAnimator anim6 = ObjectAnimator .ofFloat(phone, "scaleX",  1f);
		anim6.setDuration(1000);
		ObjectAnimator anim7 = ObjectAnimator .ofFloat(phone, "scaleY",0.5f);
		anim7.setDuration(1000);
		ObjectAnimator anim8 = ObjectAnimator .ofFloat(phone, "scaleY",  1f);
		anim8.setDuration(1000);
		
		AnimatorSet set3 = new AnimatorSet() ; 
		set3.play(anim5).before(anim6);
		AnimatorSet set2 = new AnimatorSet() ;  
		set2.play(anim2).before(set3) ;
		 
		
		AnimatorSet set4 = new AnimatorSet() ;  
		set4.play(anim7).before(anim8) ;
		AnimatorSet set5 = new AnimatorSet() ;  
		set5.play(anim4).before(set4);
		
		set.play(anim).before(set2);
		set.play(anim3).before(set5) ;
		set.start();
		set.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
				
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				set.start();
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
				// TODO Auto-generated method stub
				
			}
		});
		
		//ObjectAnimator fadeOut = ObjectAnimator.ofFloat(nfc, "alpha", 0f);
	   // ObjectAnimator mover = ObjectAnimator.ofFloat(phone, "translationX", -500f, 0f);
	   // ObjectAnimator fadeIn = ObjectAnimator.ofFloat(phone, "alpha", 0f, 1f);
	   // AnimatorSet animSet = new AnimatorSet() ;
	   // animSet.play(mover)/*.with(fadeIn).after(fadeOut)*/;
	    //animSet.start();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}


}
