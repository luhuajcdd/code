package com.android.test.annimation.animator;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;

import com.android.test.R;

public class ObjectAnimatorAlpha extends Activity {
	
	private ImageView phone, nfc ; 
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_animation_animator);
		phone =(ImageView)this.findViewById(R.id.imageView2) ;
		nfc =(ImageView)this.findViewById(R.id.imageView1) ;
		Point p = new Point() ;
		this.getWindowManager().getDefaultDisplay().getSize(p) ;
		final AnimatorSet set = new AnimatorSet() ;
		ObjectAnimator anim = ObjectAnimator.ofFloat(phone, "alpha", 1f, 0f);
		anim.setDuration(2000);
		ObjectAnimator anim2 = ObjectAnimator.ofFloat(phone, "alpha", 0f, 1f);
		anim2.setDuration(2000);
		set.play(anim).before(anim2) ;
		set.start();
		set.addListener(new AnimatorListener() {
			
			@Override
			public void onAnimationStart(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationRepeat(Animator animation) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onAnimationEnd(Animator animation) {
				// TODO Auto-generated method stub
				set.start();
			}
			
			@Override
			public void onAnimationCancel(Animator animation) {
				// TODO Auto-generated method stub
				
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}


}
