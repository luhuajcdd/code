package com.android.test.annimation.animator;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;

import com.android.test.R;

public class ObjectAnimatorSet extends Activity {
	
	private ImageView phone, nfc ; 
	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_animation_animator);
		phone =(ImageView)this.findViewById(R.id.imageView2) ;
		nfc =(ImageView)this.findViewById(R.id.imageView1) ;
		AnimatorSet set = (AnimatorSet) AnimatorInflater.loadAnimator(this,
			    R.animator.objectanimationset);
			set.setTarget(phone);
			set.start();
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}


}
