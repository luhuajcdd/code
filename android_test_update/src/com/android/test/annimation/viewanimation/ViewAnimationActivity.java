package com.android.test.annimation.viewanimation;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.android.test.R;

public class ViewAnimationActivity extends Activity {

	
	private ImageView phone, nfc ; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_animation_animator);
		phone =(ImageView)this.findViewById(R.id.imageView2) ;
		nfc =(ImageView)this.findViewById(R.id.imageView1) ;
		Animation animation = AnimationUtils.loadAnimation(this, R.anim.alpha_translate) ;
		final Animation animationnfc = AnimationUtils.loadAnimation(this, R.anim.alpha) ;
		animation.setAnimationListener(new AnimationListener() {
			
			@Override
			public void onAnimationStart(Animation animation) {
				// TODO Auto-generated method stub
			}
			
			@Override
			public void onAnimationRepeat(Animation animation) {
			}
			
			@Override
			public void onAnimationEnd(Animation animation) {
				animation.reset() ;
				animation.start() ;
			}
		});
		phone.startAnimation(animation) ;
		
		nfc.startAnimation(animationnfc) ;
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
