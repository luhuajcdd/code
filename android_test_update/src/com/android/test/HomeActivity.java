package com.android.test;

import android.app.ExpandableListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.TextView;

public class HomeActivity extends ExpandableListActivity {

    private ExpandableListView listView ; 
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.layout_home_activity) ;
	
	setListAdapter(expandableListAdapter) ;
	listView = this.getExpandableListView() ;
	listView.setOnChildClickListener(this) ;
	
    }
    
    
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.activity_main, menu);
	return true;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
            int childPosition, long id) {
	TextView tv = null; 
	if(null != v){
	    tv = (TextView)v ;
	}else{
	    return false ;
	}
	
	if(!TextUtils.isEmpty(tv.getText().toString())){
	    if("list view -- Highlight item select".trim().equals(tv.getText().toString().trim())){	//TASKMODE
		startActivity(new Intent("android.intent.action.LISTVIEW_TEST")) ;
	    }else if("listview listener transmit".trim().equals(tv.getText().toString().trim())){		//LIST
		startActivity(new Intent("android.intent.action.LISTVIEW_LISTENER_TRANSMIT")) ;
	    }else if("task mode".trim().equals(tv.getText().toString().trim())){				//Activity	
		startActivity(new Intent("android.intent.action.A")) ;
	    }else if("activity-OriginalKeyboardHiddenScreenSize".trim().equals(tv.getText().toString().trim())){
		startActivity(new Intent("android.intent.action.ORIGINAL_KEYBOARDHIDDEN_SCREENSIZE")) ;
	    }else if("customer dialog".trim().equals(tv.getText().toString().trim())){			//DIALOG
		startActivity(new Intent("android.intent.action.DIALOG_ACTIVITY_TEST")) ;
	    } else if ("sqlite test".trim().equals(tv.getText().toString().trim())) {				//SQLITE
                startActivity(new Intent("android.intent.action.SQLITE_TEST_ACTIVITY"));
	    } else if ("popup window".trim().equals(tv.getText().toString().trim())) {			//POPUP
                startActivity(new Intent("android.intent.action.POPUP_WINDOW_ACTIVITY"));
	    } else if ("popup menu".trim().equals(tv.getText().toString().trim())) {
                startActivity(new Intent("android.intent.action.POPUP_MENU_ACTIVITY"));
	    }else if("Frame".trim().equals(tv.getText().toString().trim())){					//LAYOUT
		startActivity(new Intent("android.intent.action.FRAME_LAYOUT")) ;
	    }else if("Gridlayout".trim().equals(tv.getText().toString().trim())){
		startActivity(new Intent("android.intent.action.GRID_LAYOUT")) ;
	    }else if("linear".trim().equals(tv.getText().toString().trim())){
		startActivity(new Intent("android.intent.action.LINEAR_LAYOUT")) ;
	    }else if("relative".trim().equals(tv.getText().toString().trim())){
		startActivity(new Intent("android.intent.action.RELATIVE_LAYOUT")) ;
	    }else if("table".trim().equals(tv.getText().toString().trim())){
		startActivity(new Intent("android.intent.action.TABLE_LAYOUT")) ;
	    }else if("resize".trim().equals(tv.getText().toString().trim())){
		startActivity(new Intent("android.intent.action.RELATIVELAYOUTRESIZETEST")) ;
	    }else if("Notification".trim().equals(tv.getText().toString().trim())){				//notification
		startActivity(new Intent("android.intent.action.NOTIFICATION_1")) ;
	    }else if("Canvas".trim().equals(tv.getText().toString().trim())){					//Canvas
		startActivity(new Intent("android.intent.action.CANVAS_1")) ;
	    }else if("Sign_gesture".trim().equals(tv.getText().toString().trim())){				//Sign_gesture
		startActivity(new Intent("android.intent.action.SIGN_GESTURE_TEST")) ;
	    }else if("nfc".trim().equals(tv.getText().toString().trim())){					//nfc
		startActivity(new Intent("android.intent.action.NFC_ACTIVITY_TEST")) ;
	    }else if("local".trim().equals(tv.getText().toString().trim())){					//Service
		startActivity(new Intent("android.intent.action.SERVICE_COVER")) ;
	    }else if("remote".trim().equals(tv.getText().toString().trim())){		
		startActivity(new Intent("android.intent.action.SERVICE_REMOTE_BOUND")) ;
	    }else if("Permission".trim().equals(tv.getText().toString().trim())){				//Permission	
		startActivity(new Intent("android.intent.action.ACTIVITY_PERMISSION_TEST_1")) ;
	    }else if("ContentProvider".trim().equals(tv.getText().toString().trim())){			//ContentProvider	
		startActivity(new Intent("android.intent.action.CONTENTPROVIDER_TEST")) ;
	    }else if("move".trim().equals(tv.getText().toString().trim())){					//move	
		startActivity(new Intent("android.intent.action.PICTURE_DRAFT_TEST")) ;
	    }else if("animation_home".trim().equals(tv.getText().toString().trim())){			//animation	
		startActivity(new Intent("android.intent.action.ANIMATION_HOME")) ;
	    }else if("socket".trim().equals(tv.getText().toString().trim())){					//socket	
		startActivity(new Intent("android.intent.action.SOCKET")) ;
	    }else if("AsynTask".trim().equals(tv.getText().toString().trim())){				//AsynTask	
		startActivity(new Intent("android.intent.action.ASYNTASK")) ;
	    }else if("MultiThreadDownload".trim().equals(tv.getText().toString().trim())){		//MultiThreadDownload	
		startActivity(new Intent("android.intent.action.MULTI_THREAD_DOWNLOAD")) ;
	    }else if("baiduloc".trim().equals(tv.getText().toString().trim())){					//location
	    	startActivity(new Intent("android.intent.action.BAIDU_LOCATION")) ;
	    }else if("baidumap".trim().equals(tv.getText().toString().trim())){				//map
	    	startActivity(new Intent("android.intent.action.BAIDU_MAP")) ;
	    }else if("time_like_iphone".trim().equals(tv.getText().toString().trim())){			//widget
	    	startActivity(new Intent("android.intent.action.TIME_LIKE_IPHONE")) ;
	    }else if("gesture".trim().equals(tv.getText().toString().trim())){					//gesture
	    	startActivity(new Intent("android.intent.action.GESTURE")) ;
	    }else if("drawable".trim().equals(tv.getText().toString().trim())){				//drawable
	    	startActivity(new Intent("android.intent.action.DRAWABLE")) ;
	    }else if("loader".trim().equals(tv.getText().toString().trim())){					//loader
		    startActivity(new Intent("android.intent.action.JSON_LOADER")) ;
	    };
	}
	
        return false;
    }
    
    private String[] CONSTANT_GROUPS = new String[]{"ACTIVITY","LIST","DIALOG","SQLITE","POPUP" ,
	    "LAYOUT","NOTIFICATION","CANVAS","GESTURE_SIGN","NFC","Service","Permission","ContentProvider",
	    "MOVE","animation","socket","AsynTask","MultiThreadDownload","Location","Map","Widget","Gesture",
	    "DRAWABLE", "Loader"
	    } ;
    private String[][]  CONSTANT_CHRILDS = new String[][]{
		{"task mode","activity-OriginalKeyboardHiddenScreenSize"},
		{"list view -- Highlight item select","listview listener transmit"},
		{"customer dialog"},
		{ "sqlite test" },
                {"popup window","popup menu"},
		{"Frame","Gridlayout","linear","relative","table","resize"},
		{"Notification"},
		{"Canvas"},
		{"Sign_gesture"},
		{"nfc"},
		{"local","remote"},
		{"Permission"},
		{"ContentProvider"},
		{"move"},
		{"animation_home"},
		{"socket"},
		{"AsynTask"},
		{"MultiThreadDownload"},
		{"baiduloc"},
		{"baidumap"},
		{"time_like_iphone"},
		{"gesture"},
		{"drawable"},
		{"loader"}
	} ;
    
    
    private BaseExpandableListAdapter expandableListAdapter  = new BaseExpandableListAdapter() {
        
        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
    	
    	return true;
        }
        
        @Override
        public boolean hasStableIds() {
    	
    	return true;
        }
        
        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
    	    View convertView, ViewGroup parent) {
            
            /*if(convertView != null){
        	return convertView ;
            }*/
            
            TextView textView = getGenericView();
            textView.setText(getGroup(groupPosition).toString());
            return textView;
        }
        
        public TextView getGenericView() {
            // Layout parameters for the ExpandableListView
            AbsListView.LayoutParams lp = new AbsListView.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT, 90);
            
            TextView textView = new TextView(HomeActivity.this);
            textView.setLayoutParams(lp);
            // Center the text vertically
            textView.setGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
            // Set the text starting position
            textView.setPadding(80, 0, 0, 0);
            return textView;
        }
        
        @Override
        public long getGroupId(int groupPosition) {
    	
    	return groupPosition;
        }
        
        @Override
        public int getGroupCount() {
    	
    	return CONSTANT_GROUPS.length;
        }
        
        @Override
        public Object getGroup(int groupPosition) {
    	
    	return CONSTANT_GROUPS[groupPosition];
        }
        
        @Override
        public int getChildrenCount(int groupPosition) {
    	
    	return CONSTANT_CHRILDS[groupPosition].length;
        }
        
        @Override
        public View getChildView(int groupPosition, int childPosition,
    	    boolean isLastChild, View convertView, ViewGroup parent) {
            
          /*  if(convertView != null){
        	return convertView ;
            }*/
            
            TextView textView = getGenericView();
            textView.setText(getChild(groupPosition, childPosition).toString());
            return textView;
        }
        
        @Override
        public long getChildId(int groupPosition, int childPosition) {
    	
    	return childPosition;
        }
        
        @Override
        public Object getChild(int groupPosition, int childPosition) {
    	
    	return CONSTANT_CHRILDS[groupPosition][childPosition];
        }
    };
	
}
