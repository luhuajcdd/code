package com.android.test.list;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Context;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.test.R;

public class ListViewTest extends ListActivity {

    private ListView list ;
    private static List<String> data ;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        System.out.println("onCreate");
        setContentView(R.layout.layout_listview) ;
        list = getListView() ;
        
        this.setListAdapter(adapter) ;
        
        list.setSelector(R.drawable.list_selector) ;
    }
    
    static{
	data = new ArrayList<String>();
	for(int i = 0 ; i < 30 ; i ++){
	    data.add("list item -->" + i) ;
	}
    }
    
    
    private ListAdapter adapter = new ListAdapter(){

	@Override
	public void registerDataSetObserver(DataSetObserver observer) {
	    
	}

	@Override
	public void unregisterDataSetObserver(DataSetObserver observer) {
	    
	}

	@Override
	public int getCount() {
	    return data.size();
	}

	@Override
	public Object getItem(int position) {
	    return data.get(position);
	}

	@Override
	public long getItemId(int position) {
	    return position;
	}

	@Override
	public boolean hasStableIds() {
	    return true;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
	    LayoutInflater inflater = (LayoutInflater)ListViewTest.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE) ;
	    final View view = inflater.inflate(R.layout.layout_listview_item_select_multi_1, null) ;
	    TextView tv = (TextView)view.findViewById(R.id.textView1) ;
	    tv.setText(data.get(position)) ;
	    CheckBox cb = (CheckBox)view.findViewById(R.id.checkBox1) ;
	    cb.setOnClickListener(new View.OnClickListener() {
		@Override
		public void onClick(View v) {
		    CheckBox cb = (CheckBox)v ;
		    if(cb.isChecked()){
			view.setBackgroundResource(R.drawable.list_selector2) ;
		    }else{
			view.setBackgroundResource(R.drawable.list_selector3) ;
		    }
		}
	    }) ;
	    return view;
	}

	@Override
	public int getItemViewType(int position) {
	    return 0;
	}

	@Override
	public int getViewTypeCount() {
	    return 1;
	}

	@Override
	public boolean isEmpty() {
	    return false;
	}

	@Override
	public boolean areAllItemsEnabled() {
	    return true;
	}

	@Override
	public boolean isEnabled(int position) {
	    return true;
	}
	
    } ;
    
    @Override
    protected void onListItemClick(ListView l, View view, int position, long id) {
	System.out.println("onListItemClick()");
	TextView tv = (TextView)view.findViewById(R.id.textView1) ;
	tv.setText(data.get(position)) ;
	CheckBox cb = (CheckBox)view.findViewById(R.id.checkBox1) ;
    }
    
    @Override
    protected void onRestart() {
        super.onRestart();
        System.out.println("onRestart()");
    }

    
    @Override
    protected void onResume() {
        super.onResume();
        System.out.println("onResume()");
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        System.out.println("onStart()");
    }
    
    @Override
    public void onContentChanged() {
        super.onContentChanged();
        System.out.println("onContentChanged()");
    }
}
