package com.android.test.list.optimize;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.os.Bundle;

import com.android.test.R;

public class OptimizeListView extends ListActivity{
	
	private OptimizeAdapter mAdapter ; 
	private static List<User> users ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_listview) ;
		
		mAdapter = new OptimizeAdapter(users , this) ;
		
		setListAdapter(mAdapter) ;
		
	}
	
	static{
		users = new ArrayList<User>() ;
		for(int i = 0 ; i < 30 ; i ++){
			User user = new User() ;
			user.id = i ;
			user.name = "name" + i ;
			user.age = (short) (i + 20) ;
			users.add(user) ;
		}
	}
	
}
