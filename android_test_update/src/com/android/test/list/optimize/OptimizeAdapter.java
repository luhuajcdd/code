package com.android.test.list.optimize;

import java.util.List;

import com.android.test.R;
import com.android.test.log.LogCore;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class OptimizeAdapter extends BaseAdapter {

	private final String TAG = OptimizeAdapter.class.getSimpleName() ;
	
	private List<User> users ;
	private OptimizeListView optimizeListView ;
	private LayoutInflater mInflater ;
	
	public OptimizeAdapter(List<User> users, OptimizeListView optimizeListView) {
		this.users = users ;
		this.optimizeListView = optimizeListView ;
		mInflater = LayoutInflater.from(this.optimizeListView) ;
	}

	@Override
	public int getCount() {
		return users.size() ;
	}

	@Override
	public Object getItem(int position) {
		return users.get(position) ;
	}

	@Override
	public long getItemId(int position) {
		return position ;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView ;
		ViewHolder holder = null ;
		if(view == null){
			LogCore.logE(TAG, "position = " + position + " ; " + convertView) ;
			holder = new ViewHolder() ;
			view = mInflater.inflate(R.layout.layout_list_item_three_textview, null) ;
			holder.id = (TextView) view.findViewById(R.id.id) ;
			holder.name = (TextView) view.findViewById(R.id.name) ;
			holder.age = (TextView) view.findViewById(R.id.age) ;
			view.setTag(holder) ;
		}else{
			LogCore.logE(TAG, "position = " + position + " ; " + convertView) ;
		}
		holder = (ViewHolder) view.getTag() ;
		bindData(holder, position) ;
		return view;
	}

	private void bindData(ViewHolder holder, int position) {
		User user = users.get(position) ;
		holder.id.setText(user.id + "") ;
		holder.name.setText(user.name + "") ;
		holder.age.setText(user.age + "") ;
	}

	static class ViewHolder{
		TextView id, name, age ;
	}
	
}
