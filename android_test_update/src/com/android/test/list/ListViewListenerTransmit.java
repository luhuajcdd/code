package com.android.test.list;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.ListActivity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.android.test.R;

public class ListViewListenerTransmit extends ListActivity {

	private ListView list ;
	private ListAdapter adapter ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_listview_listener_transmit);
		list = getListView() ;
		initAdapter() ;
		//list.setAdapter(adapter) ;
		
	}

	private void initAdapter() {
		final List<Map<String ,String >> data = new ArrayList<Map<String, String>>() ;
		for(int i = 0 ; i < 20 ; i ++){
			Map<String, String> map = new HashMap<String, String>() ;
			map.put("text", "text" + i) ;
			map.put("button", "button" + i) ;
			data.add(map);
		}
		
		MyAdapter adapter2 = new MyAdapter(this, data, R.layout.layout_list_item, new String[]{"text","button"}, new int[]{R.id.textView1, R.id.button1}) ;
		setListAdapter(adapter2) ;
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	/**
     * This class can be used by external clients of SimpleAdapter to bind
     * values to views.
     *
     * You should use this class to bind values to views that are not
     * directly supported by SimpleAdapter or to change the way binding
     * occurs for views supported by SimpleAdapter.
     *
     * @see SimpleAdapter#setViewImage(ImageView, int)
     * @see SimpleAdapter#setViewImage(ImageView, String)
     * @see SimpleAdapter#setViewText(TextView, String)
     */
    public static interface ViewBinder {
        /**
         * Binds the specified data to the specified view.
         *
         * When binding is handled by this ViewBinder, this method must return true.
         * If this method returns false, SimpleAdapter will attempts to handle
         * the binding on its own.
         *
         * @param view the view to bind the data to
         * @param data the data to bind to the view
         * @param textRepresentation a safe String representation of the supplied data:
         *        it is either the result of data.toString() or an empty String but it
         *        is never null
         *
         * @return true if the data was bound to the view, false otherwise
         */
        boolean setViewValue(View view, Object data, String textRepresentation);
    }

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		System.out.println("item Click " + position + "row id " + id  );
	}
	
	class MyAdapter extends BaseAdapter  implements Filterable {

		private int[] mTo;
	    private String[] mFrom;
	    private ViewBinder mViewBinder;

	    private List<? extends Map<String, ?>> mData;

	    private int mResource;
	    private int mDropDownResource;
	    private LayoutInflater mInflater;

	    private SimpleFilter mFilter;
	    private ArrayList<Map<String, ?>> mUnfilteredData;
		
		public MyAdapter(Context context, List<? extends Map<String, ?>> data,
            int resource, String[] from, int[] to){
			mData = data;
	        mResource = mDropDownResource = resource;
	        mFrom = from;
	        mTo = to;
	        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		}
		
		   
		@Override
		public boolean isEmpty() {
			return super.isEmpty();
		}
		
		@Override
		public int getViewTypeCount() {
			return super.getViewTypeCount();
		}
		
		@Override
		public boolean hasStableIds() {
			return super.hasStableIds();
		}
		
		@Override
		public int getItemViewType(int position) {
			return 1;
		}
		
		@Override
		public boolean areAllItemsEnabled() {
			return false;
		}
		
		@Override
		public boolean isEnabled(int position) {
			if(position % 2 == 0){
				return false ;
			}
			return super.isEnabled(position);
		}
		
	    /**
	     * @see android.widget.Adapter#getCount()
	     */
	    public int getCount() {
	        return mData.size();
	    }

	    /**
	     * @see android.widget.Adapter#getItem(int)
	     */
	    public Object getItem(int position) {
	        return mData.get(position);
	    }

	    /**
	     * @see android.widget.Adapter#getItemId(int)
	     */
	    public long getItemId(int position) {
	        return position;
	    }

	    /**
	     * @see android.widget.Adapter#getView(int, View, ViewGroup)
	     */
	    public View getView(int position, View convertView, ViewGroup parent) {
	        return createViewFromResource(position, convertView, parent, mResource);
	    }

	    private View createViewFromResource(int position, View convertView,
	            ViewGroup parent, int resource) {
	        View v;
	        if (convertView == null) {
	            v = mInflater.inflate(resource, parent, false);
	        } else {
	            v = convertView;
	        }

	        bindView(v,position);

	        return v;
	    }

	    /**
	     * <p>Sets the layout resource to create the drop down views.</p>
	     *
	     * @param resource the layout resource defining the drop down views
	     * @see #getDropDownView(int, android.view.View, android.view.ViewGroup)
	     */
	    public void setDropDownViewResource(int resource) {
	        this.mDropDownResource = resource;
	    }

	    @Override
	    public View getDropDownView(int position, View convertView, ViewGroup parent) {
	        return createViewFromResource(position, convertView, parent, mDropDownResource);
	    }

	    private void bindView(View view,int position) {
			System.out.println("bindView() " + position);
			TextView tv = (TextView)view.findViewById(R.id.textView1) ;
			Button b = (Button)view.findViewById(R.id.button1) ;
			final String button1 = (String) mData.get(position).get("button") ;
			tv.setText((String)mData.get(position).get("text")) ;
			b.setText(button1) ;
			b.setOnClickListener(new View.OnClickListener(
					) {
				
				@Override
				public void onClick(View v) {
					System.out.println("click on button " + button1);
				}
			}) ;
		};

	    /**
	     * Returns the {@link ViewBinder} used to bind data to views.
	     *
	     * @return a ViewBinder or null if the binder does not exist
	     *
	     * @see #setViewBinder(android.widget.SimpleAdapter.ViewBinder)
	     */
	    public ViewBinder getViewBinder() {
	        return mViewBinder;
	    }

	    /**
	     * Sets the binder used to bind data to views.
	     *
	     * @param viewBinder the binder used to bind data to views, can be null to
	     *        remove the existing binder
	     *
	     * @see #getViewBinder()
	     */
	    public void setViewBinder(ViewBinder viewBinder) {
	        mViewBinder = viewBinder;
	    }

	    /**
	     * Called by bindView() to set the image for an ImageView but only if
	     * there is no existing ViewBinder or if the existing ViewBinder cannot
	     * handle binding to an ImageView.
	     *
	     * This method is called instead of {@link #setViewImage(ImageView, String)}
	     * if the supplied data is an int or Integer.
	     *
	     * @param v ImageView to receive an image
	     * @param value the value retrieved from the data set
	     *
	     * @see #setViewImage(ImageView, String)
	     */
	    public void setViewImage(ImageView v, int value) {
	        v.setImageResource(value);
	    }

	    /**
	     * Called by bindView() to set the image for an ImageView but only if
	     * there is no existing ViewBinder or if the existing ViewBinder cannot
	     * handle binding to an ImageView.
	     *
	     * By default, the value will be treated as an image resource. If the
	     * value cannot be used as an image resource, the value is used as an
	     * image Uri.
	     *
	     * This method is called instead of {@link #setViewImage(ImageView, int)}
	     * if the supplied data is not an int or Integer.
	     *
	     * @param v ImageView to receive an image
	     * @param value the value retrieved from the data set
	     *
	     * @see #setViewImage(ImageView, int) 
	     */
	    public void setViewImage(ImageView v, String value) {
	        try {
	            v.setImageResource(Integer.parseInt(value));
	        } catch (NumberFormatException nfe) {
	            v.setImageURI(Uri.parse(value));
	        }
	    }

	    /**
	     * Called by bindView() to set the text for a TextView but only if
	     * there is no existing ViewBinder or if the existing ViewBinder cannot
	     * handle binding to a TextView.
	     *
	     * @param v TextView to receive text
	     * @param text the text to be set for the TextView
	     */
	    public void setViewText(TextView v, String text) {
	        v.setText(text);
	    }

	    public Filter getFilter() {
	        if (mFilter == null) {
	            mFilter = new SimpleFilter();
	        }
	        return mFilter;
	    }

	  

	    /**
	     * <p>An array filters constrains the content of the array adapter with
	     * a prefix. Each item that does not start with the supplied prefix
	     * is removed from the list.</p>
	     */
	    private class SimpleFilter extends Filter {

	        @Override
	        protected FilterResults performFiltering(CharSequence prefix) {
	            FilterResults results = new FilterResults();

	            if (mUnfilteredData == null) {
	                mUnfilteredData = new ArrayList<Map<String, ?>>(mData);
	            }

	            if (prefix == null || prefix.length() == 0) {
	                ArrayList<Map<String, ?>> list = mUnfilteredData;
	                results.values = list;
	                results.count = list.size();
	            } else {
	                String prefixString = prefix.toString().toLowerCase();

	                ArrayList<Map<String, ?>> unfilteredValues = mUnfilteredData;
	                int count = unfilteredValues.size();

	                ArrayList<Map<String, ?>> newValues = new ArrayList<Map<String, ?>>(count);

	                for (int i = 0; i < count; i++) {
	                    Map<String, ?> h = unfilteredValues.get(i);
	                    if (h != null) {
	                        
	                        int len = mTo.length;

	                        for (int j=0; j<len; j++) {
	                            String str =  (String)h.get(mFrom[j]);
	                            
	                            String[] words = str.split(" ");
	                            int wordCount = words.length;
	                            
	                            for (int k = 0; k < wordCount; k++) {
	                                String word = words[k];
	                                
	                                if (word.toLowerCase().startsWith(prefixString)) {
	                                    newValues.add(h);
	                                    break;
	                                }
	                            }
	                        }
	                    }
	                }

	                results.values = newValues;
	                results.count = newValues.size();
	            }

	            return results;
	        }

	        @Override
	        protected void publishResults(CharSequence constraint, FilterResults results) {
	            //noinspection unchecked
	            mData = (List<Map<String, ?>>) results.values;
	            if (results.count > 0) {
	                notifyDataSetChanged();
	            } else {
	                notifyDataSetInvalidated();
	            }
	        }
	    }
		
	}
	
	
}
