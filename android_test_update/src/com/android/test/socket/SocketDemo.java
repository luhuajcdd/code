package com.android.test.socket;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.test.R;
/**
 * 除了isClose方法，Socket类还有一个isConnected方法来判断Socket对象是否连接成功。
 * 看到这个名字，也许读者会产生误解。
 * 其实isConnected方法所判断的并不是Socket对象的当前连接状态，
 * 而是Socket对象是否曾经连接成功过，如果成功连接过，即使现在isClose返回true，
 * isConnected仍然返回true。因此，要判断当前的Socket对象是否处于连接状态，
 * 必须同时使用isClose和isConnected方法，
 * 即只有当isClose返回false，isConnected返回true的时候Socket对象才处于连接状态。
 * 虽然在大多数的时候可以直接使用Socket类或输入输出流的close方法关闭网络连接，
 * 但有时我们只希望关闭OutputStream或InputStream，而在关闭输入输出流的同时，并不关闭网络连接。
 * 这就需要用到Socket类的另外两个方法：shutdownInput和shutdownOutput，
 * 这两个方法只关闭相应的输入、输出流，而它们并没有同时关闭网络连接的功能。
 * 和isClosed、isConnected方法一样，
 * Socket类也提供了两个方法来判断Socket对象的输入、输出流是否被关闭，
 * 这两个方法是isInputShutdown()和isOutputShutdown()。
 * shutdownInput和shutdownOutput并不影响Socket对象的状态。 
 */
//====================================================================================
/**
 * 运行过程剖析：
 * 1，服务器启动   
 * 2，客户端与服务器连接成功
 * 3，服务器发送信息给客户端 的线程 
 * 4，线程通过Handler发送信息给UI线程
 * 5，用TextView把信息显示
 * 
 * 当点击按钮时流程为：
 * 1，发送消息给服务器
 * 2，服务器受到客户端发送来的信息，并给服务器返回信息
 * 如上。
 */
public class SocketDemo extends Activity implements Runnable {
	private TextView tv_msg = null;
	private EditText ed_msg = null;
	private Button btn_send = null;
	// private Button btn_login = null;
	private static final String HOST = "192.168.18.19";
	private static final int PORT = 9999;
	private Socket socket = null;
	private BufferedReader in = null;
	private PrintWriter out = null;
	private String content = "";
	//接收线程发送过来信息，并用TextView显示
	public Handler mHandler = new Handler() {
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			tv_msg.setText(tv_msg.getText() + "\r\n" + content);
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_socket);

		tv_msg = (TextView) findViewById(R.id.TextView);
		ed_msg = (EditText) findViewById(R.id.EditText01);
		btn_send = (Button) findViewById(R.id.Button02);
		new Thread(SocketDemo.this).start();
		btn_send.setOnClickListener(new Button.OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String msg = ed_msg.getText().toString();
				if (socket.isConnected()) {
					if (!socket.isOutputShutdown()) {
						out.println(msg);
					}
				}
			}
		});
	}
	/**
	 * 读取服务器发来的信息，并通过Handler发给UI线程
	 */
	public void run() {
		try {
			socket = new Socket(HOST, PORT);
			in = new BufferedReader(new InputStreamReader(socket
					.getInputStream()));
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
					socket.getOutputStream())), true);
		} catch (IOException ex) {
		}
		try {
			while (true) {
				if (!socket.isClosed()) {
					if (socket.isConnected()) {
						if (!socket.isInputShutdown()) {
							if ((content = in.readLine()) != null) {
								content += "\n";
								mHandler.sendMessage(mHandler.obtainMessage());
							} else {

							}
						}
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		if (socket.isConnected()) {
			if (!socket.isOutputShutdown()) {
				out.println("exit");
			}
		}
	}

}