package com.android.test.nfc;

import android.animation.Animator;
import android.animation.Animator.AnimatorListener;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Point;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.widget.ImageView;

import com.android.test.R;

public class NFCActivity extends Activity {

    protected NfcAdapter mAdapter;
    protected PendingIntent mPendingIntent;
    protected IntentFilter[] mFilters;
    protected String[][] mTechLists;

    private ImageView phone, nfc;

    private Dialog dialog;
    private ProgressDialog mProgressDialog;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.layout_nfc_main);
	phone = (ImageView) this.findViewById(R.id.phone);
	nfc = (ImageView) this.findViewById(R.id.radio);
	final AnimatorSet set = new AnimatorSet();
	Point p = new Point();
	this.getWindowManager().getDefaultDisplay().getSize(p);
	ObjectAnimator anim2 = ObjectAnimator.ofFloat(phone, "translationY",
		0f, 0 - p.y / 2);
	anim2.setDuration(2000);
	ObjectAnimator anim4 = ObjectAnimator.ofFloat(phone, "translationY",
		0 - p.y / 2, 0f);
	anim4.setDuration(2000);
	ObjectAnimator anim = ObjectAnimator.ofFloat(nfc, "alpha", 1f, 0f);
	anim.setDuration(1000);
	ObjectAnimator anim3 = ObjectAnimator.ofFloat(nfc, "alpha", 0f, 1f);
	anim2.setDuration(1000);

	AnimatorSet set2 = new AnimatorSet();
	set2.play(anim3).before(anim4);
	AnimatorSet set5 = new AnimatorSet();
	set5.play(anim).before(set2);
	AnimatorSet set4 = new AnimatorSet();
	set4.play(anim3).before(set5);
	AnimatorSet set3 = new AnimatorSet();
	set3.play(anim).before(set4);
	set.play(anim2).before(set3);
	set.start();
	set.addListener(new AnimatorListener() {

	    @Override
	    public void onAnimationStart(Animator animation) {

	    }

	    @Override
	    public void onAnimationRepeat(Animator animation) {

	    }

	    @Override
	    public void onAnimationEnd(Animator animation) {
		set.start();
	    }

	    @Override
	    public void onAnimationCancel(Animator animation) {

	    }
	});

	mAdapter = NfcAdapter.getDefaultAdapter(this);
	mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
		getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
    }

    private void checkNFCStatus() {
	NfcManager manager = (NfcManager) this
		.getSystemService(Context.NFC_SERVICE);
	NfcAdapter adapter = manager.getDefaultAdapter();
	if (adapter != null && adapter.isEnabled()) {
	} else {
	    dialog = new AlertDialog.Builder(this)
		    .setTitle(null)
		    .setMessage(getString(R.string.openNFC))
		    .setPositiveButton(getString(R.string.confirm),
			    new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog,
					int which) {
				    try {
					startActivity(new Intent(
						android.provider.Settings.ACTION_WIRELESS_SETTINGS));
				    } catch (android.content.ActivityNotFoundException e) {
				    }
				}
			    }).show();
	}
    }

    @Override
    protected void onResume() {
	super.onResume();
	// Check NFC status
	if (dialog != null) {
	    dialog.dismiss();
	}
	checkNFCStatus();
	mAdapter.enableForegroundDispatch(this, mPendingIntent, null,
		mTechLists);
    }

    @Override
    protected void onNewIntent(final Intent intent) {
	mProgressDialog = ProgressDialog.show(this,
		this.getString(R.string.reading),
		getString(R.string.reading_1_MK), true, false);
	// Handl reading
	read(intent);
	mProgressDialog.dismiss();
	this.finish();
    }

    private void read(Intent intent) {
	Tag mTagFromIntent = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
	if (mTagFromIntent != null) {
	    byte[] data = mTagFromIntent.getId();
	    String uid = NFCParser.hexToASCIIDirect(data);
	    sendBroadcastToReceiver(uid);
	}
    }

    private void sendBroadcastToReceiver(String str) {
	Intent intent = new Intent(NFCConstants.BROADCASTRECEIVE_ACTION);
	intent.putExtra(NFCConstants.DATA, str);
	sendBroadcast(intent);
    }

    @Override
    public void onPause() {
	super.onPause();
	mAdapter.disableForegroundDispatch(this);
    }

}
