package com.android.test.nfc;

/**
 * @ClassName: NFCParser
 * @author:hlu
 * @Date�?012-8-22
 * @version  V1.0.0
 * @Description: Parser a hex to ascii
 */
public class NFCParser {

	public char  hexToASCII(byte b){
		if (b>=0&&b<=9){
			byte b3 = (byte)((b) + 0x30);
			return (char)b3 ;
		}else if(b>=10&&b<=15){
			byte b3 = (byte)((b-0x0A) + 0x41);
			return (char)b3 ;
		}
		throw new IllegalArgumentException("unknow argument.") ;
	}
	
	public static String hexToASCIIDirect(byte[] bytes){
		StringBuffer sb = new StringBuffer("") ;
		for (byte b : bytes) {
			byte b1 = (byte)((b>>4)&0x0f);
			parserHexToASCII(sb, b1);
			
			byte b2 = (byte)(b & 0x0f) ; 
			parserHexToASCII(sb, b2);
		}
		return sb.toString() ;
	}

	private static void parserHexToASCII(StringBuffer sb, byte b1) {
		if (b1>=0&&b1<=9){
			byte b3 = (byte)((b1) + 0x30);
			sb.append((char)b3) ;
		}
		else if(b1>=10&&b1<=15){
			byte b3 = (byte)((b1-0x0A) + 0x41);
			sb.append((char)b3) ;
		}
	}
	
}
