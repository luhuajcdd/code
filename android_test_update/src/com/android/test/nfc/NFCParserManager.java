package com.android.test.nfc;

import android.content.Intent;
import android.nfc.NfcAdapter;
import android.nfc.Tag;

/**
 * @ClassName: NFCParserManager
 * @author:hlu
 * @Date�?012-8-22
 * @version  V1.0.0
 * @Description: Tag parser manager, control how to parser tag
 */
public class NFCParserManager {

   private final String NFCA = "android.nfc.tech.NfcA";
   private final String NFCB = "android.nfc.tech.NfcB";
   private final String NFCV = "android.nfc.tech.NfcV";
   private final String NFCF = "android.nfc.tech.NfcF";
   private final String NDEF = "android.nfc.tech.Ndef";

   public String paserManager(Intent intent) {
	  String action = intent.getAction();
	  if (action == null) {
		 return null;
	  }
	  Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
      
	  if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)) {
		 if (tag != null) {
			return getTagInfo(tag);
		 }
	  } else if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
		 if (tag != null) {
			return getTagInfo(tag);
		 }
	  } else if (NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)) {
		 if (tag != null) {
			return getTagInfo(tag);
		 }
	  }

	  return null;
   }

   private String getTagInfo(Tag tag) {
	  byte[] bytes = tag.getId();
	  String t = new String(bytes);
	  String[] techList = tag.getTechList();
	  for (String tech : techList) {
		 if (NFCA.equals(tech)) {
			String tagInfo = NFCParser.hexToASCIIDirect(bytes);
			return "".equals(tagInfo) ? null : tagInfo;
		 } else if (NFCB.equals(tech)) {
			String tagInfo = NFCParser.hexToASCIIDirect(bytes);
			return "".equals(tagInfo) ? null : tagInfo;
		 } else if (NFCF.equals(tech)) {
			String tagInfo = NFCParser.hexToASCIIDirect(bytes);
			return "".equals(tagInfo) ? null : tagInfo;
		 } else if (NFCV.equals(tech)) {
			String tagInfo = NFCParser.hexToASCIIDirect(bytes);
			return tagInfo;
/*			StringBuffer tagInfoTemp = new StringBuffer(tagInfo).reverse();
			 System.out.println("---3----:"+tagInfoTemp.toString());
			tagInfo = reverseEachTwoChar(tagInfoTemp) ;
			return "".equals(tagInfo) ? null : tagInfo;*///与新标签不符合，先注释掉
		 } else if (NDEF.equals(tech)) {
			String tagInfo = NFCParser.hexToASCIIDirect(bytes);
			return "".equals(tagInfo) ? null : tagInfo;
		 }
	  }
	  return null;
   }

	/**
	 * @Description: Reverse Each two char
	 * @param tagInfoTemp
	 * @return String
	 */
	private String reverseEachTwoChar(StringBuffer tagInfoTemp) {
		StringBuffer tagInfo = new StringBuffer() ;
		String tagStr = tagInfoTemp.toString() ;
		int length = tagStr.length() ;
		for(int i = 0 ; i < length/2; i ++){
			String sub = tagInfoTemp.substring(2*i , 2*(i+1)) ;
			tagInfo.append(new StringBuffer(sub).reverse()) ;
		}
		return tagInfo.toString();
	}

}
