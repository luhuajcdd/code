package com.android.test.nfc;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.test.R;

public class NFCActivityTest extends Activity {
    private TextView text ;
    private NFCReceiver receiver ;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.layout_nfc_test);
	text = (TextView)findViewById(R.id.text) ;
	
	receiver = new NFCReceiver() ;
	IntentFilter filter = new IntentFilter() ;
	filter.addAction(NFCConstants.BROADCASTRECEIVE_ACTION) ;
	filter.addCategory("android.intent.category.DEFAULT") ;
	this.registerReceiver(receiver, filter) ;
	
	findViewById(R.id.start_nfc).setOnClickListener(new View.OnClickListener(){

	    @Override
	    public void onClick(View v) {
		startActivity(new Intent("android.intent.action.NFC_ACTIVITY")) ;
	    }
	    
	}) ;
	
	
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	getMenuInflater().inflate(R.menu.activity_main, menu);
	return true;
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }

    class NFCReceiver extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
	    if(null != intent){
		String data = intent.getStringExtra(NFCConstants.DATA) ;
		text.setText(text.getText().toString()+data) ;
		Toast.makeText(getApplicationContext(), getApplicationContext().getString(R.string.success), Toast.LENGTH_SHORT).show() ;
	    }
	}
	
    }
}
