package com.android.test.drawable;

import android.graphics.drawable.TransitionDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;

import com.android.test.R;

public class DrawableXMLActivity extends FragmentActivity {
	@Override
	protected void onCreate(Bundle arg0) {
		super.onCreate(arg0);
		setContentView(R.layout.layout_drawable) ;
		ImageView button = (ImageView) findViewById(R.id.imageView2);
		TransitionDrawable drawable = (TransitionDrawable) button.getDrawable();
		drawable.startTransition(500);
	}
}
