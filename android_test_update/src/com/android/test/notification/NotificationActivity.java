package com.android.test.notification;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.View;

import com.android.test.R;

public class NotificationActivity extends Activity implements
	View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.layout_notification);

	findViewById(R.id.notification1).setOnClickListener(this);
	findViewById(R.id.big_style).setOnClickListener(this);
	findViewById(R.id.big_picture_style).setOnClickListener(this);
	findViewById(R.id.big_text_style).setOnClickListener(this);
	findViewById(R.id.in_box_style).setOnClickListener(this);
	findViewById(R.id.regular_activity_PendingIntent).setOnClickListener(this);
	findViewById(R.id.special_activity_PendingIntent).setOnClickListener(this);
	findViewById(R.id.progress_notification).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.notification1:
	    notificationSimple();
	    break;
	case R.id.big_style:
	    notificationBigStyle();
	    break;
	case R.id.big_picture_style:
	    notificationBigPictureStyle();
	    break;
	case R.id.big_text_style:
	    notificationBigTextStyle();
	    break;
	case R.id.in_box_style:
	    notificationInBoxStyle() ;
	case R.id.regular_activity_PendingIntent:
	    notificationRegular() ;
	case R.id.special_activity_PendingIntent:
	    notificationSpecial() ;
	case R.id.progress_notification:
	    notificationProgress() ;
	}

    }

    private void notificationProgress() {
	final NotificationManager mNotifyManager =
	        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	final NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this);
	mBuilder.setContentTitle("Picture Download")
	    .setContentText("Download in progress")
	    .setSmallIcon(android.R.drawable.ic_dialog_map);
	// Start a lengthy operation in a background thread
	new Thread(
	    new Runnable() {
	        @Override
	        public void run() {
	            int incr;
	            mBuilder.setProgress(0, 0, true);
	            mNotifyManager.notify(6, mBuilder.build());
	            try {
                        // Sleep for 5 seconds
                        Thread.sleep(5*1000);
                    } catch (InterruptedException e) {
                        Log.d("", "sleep failure");
                    }
	            // Do the "lengthy" operation 20 times
	            for (incr = 0; incr <= 100; incr+=5) {
	                    // Sets the progress indicator to a max value, the
	                    // current completion percentage, and "determinate"
	                    // state
	                    mBuilder.setProgress(100, incr, false);
	                    // Displays the progress bar for the first time.
	                    mNotifyManager.notify(6, mBuilder.build());
	                        // Sleeps the thread, simulating an operation
	                        // that takes time
	                        try {
	                            // Sleep for 5 seconds
	                            Thread.sleep(5*1000);
	                        } catch (InterruptedException e) {
	                            Log.d("", "sleep failure");
	                        }
	            }
	            // When the loop is finished, updates the notification
	            mBuilder.setContentText("Download complete")
	            // Removes the progress bar
	                    .setProgress(0,0,false);
	            mNotifyManager.notify(6, mBuilder.build());
	        }
	    }
	// Starts the thread by calling the run() method in its Runnable
	).start();	
    }

    private void notificationSpecial() {
	// Instantiate a Builder object.
	NotificationCompat.Builder builder = new NotificationCompat.Builder(
		this).setAutoCancel(true)
		.setContentTitle("DJ-Android notification")
		.setSmallIcon(R.drawable.ic_launcher)
		.setContentText("Hello World!");
	// Creates an Intent for the Activity
	Intent intent = new Intent("android.intent.action.NOTIFICATION_2");
	// Sets the Activity to start in a new, empty task
	intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
	// Creates the PendingIntent
	PendingIntent notifyIntent =
	        PendingIntent.getActivity(
	        this.getApplicationContext(),
	        0,
	        intent,
	        PendingIntent.FLAG_UPDATE_CURRENT
	);

	// Puts the PendingIntent into the notification builder
	builder.setContentIntent(notifyIntent);
	// Notifications are issued by sending them to the
	// NotificationManager system service.
	NotificationManager mNotificationManager =
	    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	// Builds an anonymous Notification object from the builder, and
	// passes it to the NotificationManager
	mNotificationManager.notify(4, builder.build());
    }

    private void notificationRegular() {
	Intent resultIntent = new Intent(this, NotificationActivity3.class);
	TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
	// Adds the back stack
	stackBuilder.addParentStack(NotificationActivity3.class);
	// Adds the Intent to the top of the stack
	stackBuilder.addNextIntent(resultIntent);
	// Gets a PendingIntent containing the entire back stack
	PendingIntent resultPendingIntent =
	        stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
	
	NotificationCompat.Builder builder =  new NotificationCompat.Builder(
		this).setSmallIcon(android.R.drawable.ic_dialog_email)
		.setContentTitle("My notification")
		.setContentText("Hello World!")
		.setVibrate(new long[] { 1000, 2000 }).setAutoCancel(true)
		.setPriority(0);
	builder.setContentIntent(resultPendingIntent);
	NotificationManager mNotificationManager =
		    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	mNotificationManager.notify(5, builder.build());
    }

    @SuppressLint("NewApi")
    private void notificationInBoxStyle() {
	Bitmap icon1 = BitmapFactory.decodeResource(getResources(),
		android.R.drawable.ic_dialog_email);
	Builder builder = new Notification.Builder(this);
	builder.setSmallIcon(R.drawable.ic_launcher);
	builder.setTicker("3 new messages from Chitranshu");
	builder.setContentTitle("3 new messages from Chitranshu");
	builder.setContentText("+ 5 more");
	builder.setLargeIcon(icon1);
	builder.setAutoCancel(true);

	Notification.InboxStyle inboxStyle = new Notification.InboxStyle(builder);
	inboxStyle.setBigContentTitle("3 new messages from Chitranshu");
	inboxStyle.setSummaryText("+ 5 more");
	((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).notify(03, inboxStyle.build());
    }

    private void notificationBigTextStyle() {
	Bitmap icon1 = BitmapFactory.decodeResource(getResources(),
		android.R.drawable.ic_dialog_email);
	NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
		this).setAutoCancel(true)
		.setContentTitle("DJ-Android notification")
		.setSmallIcon(R.drawable.ic_launcher).setLargeIcon(icon1)
		.setContentText("Hello World!");
	NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
	bigText.bigText("Android is a Linux-based operating system designed primarily for touchscreen"
		+ " mobile devices such as smartphones and tablet computers. Initially developed by Android,"
		+ " Inc., which Google backed financially and later bought in 2005,[12] Android was unveiled in"
		+ " 2007 along with the founding of the Open Handset Alliance: a consortium of hardware, software,"
		+ " and telecommunication companies devoted to advancing open standards for mobile devices."
		+ "[13] The first Android-powered phone was sold in October 2008");
	bigText.setBigContentTitle("Android");
	bigText.setSummaryText("By: Dhaval Sodha Parmar");
	mBuilder.setStyle(bigText);
	// Creates an explicit intent for an Activity in your app
	Intent resultIntent = new Intent(this, NotificationActivity2.class);
	// The stack builder object will contain an artificial back stack for //
	// the // started Activity.
	// This ensures that navigating backward from the Activity leads out of
	// your application to the Home screen.
	TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
	// Adds the back stack for the Intent (but not the Intent itself)
	stackBuilder.addParentStack(NotificationActivity.class);
	// Adds the Intent that starts the Activity to the top of the stack
	stackBuilder.addNextIntent(resultIntent);
	PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
		PendingIntent.FLAG_UPDATE_CURRENT);
	mBuilder.setContentIntent(resultPendingIntent);
	NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	// mId allows you to update the notification later on.
	mNotificationManager.notify(101, mBuilder.build());
    }

    @SuppressLint("NewApi")
    private void notificationBigPictureStyle() {
	Bitmap icon1 = BitmapFactory.decodeResource(getResources(),
		R.drawable.mv);
	Notification.Builder mBuilder = new Notification.Builder(
		this).setAutoCancel(true)
		.setContentTitle("DJ-Android notification")
		.setSmallIcon(R.drawable.ic_launcher).setLargeIcon(icon1)
		.setContentText("Hello World!");
	Notification.BigPictureStyle bigPicStyle = new Notification.BigPictureStyle();
	bigPicStyle.bigPicture(icon1);
	bigPicStyle.setBigContentTitle("Dhaval Sodha Parmar");
	mBuilder.setStyle(bigPicStyle);
	// Creates an explicit intent for an Activity in your app
	Intent resultIntent = new Intent(this, NotificationActivity2.class);
	// The stack builder object will contain an artificial back stack for //
	// the // started Activity.
	// This ensures that navigating backward from the Activity leads out of
	// your application to the Home screen.
	TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
	// Adds the back stack for the Intent (but not the Intent itself)
	stackBuilder.addParentStack(NotificationActivity.class);
	// Adds the Intent that starts the Activity to the top of the stack
	stackBuilder.addNextIntent(resultIntent);
	PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
		PendingIntent.FLAG_UPDATE_CURRENT);
	mBuilder.setContentIntent(resultPendingIntent);
	NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	// mId allows you to update the notification later on.
	mNotificationManager.notify(100, mBuilder.build());

    }

    private void notificationBigStyle() {
	NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
		this).setSmallIcon(android.R.drawable.ic_media_play)
		.setContentTitle("Event tracker")
		.setContentText("Events received")
		.setOngoing(true);
	NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
	String[] events = new String[3];
	// Sets a title for the Inbox style big view
	inboxStyle.setBigContentTitle("Event tracker details:");
	// Moves events into the big view
	for (int i = 0; i < events.length; i++) {

	    inboxStyle.addLine("  --- i = " + i);
	}
	inboxStyle.setSummaryText("+3 more");
	// Moves the big view style object into the notification object.
	mBuilder.setStyle(inboxStyle);
	// Creates an explicit intent for an Activity in your app
	Intent resultIntent = new Intent(this, NotificationActivity2.class);
	// The stack builder object will contain an artificial back stack for //
	// the // started Activity.
	// This ensures that navigating backward from the Activity leads out of
	// your application to the Home screen.
	TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
	// Adds the back stack for the Intent (but not the Intent itself)
	stackBuilder.addParentStack(NotificationActivity.class);
	// Adds the Intent that starts the Activity to the top of the stack
	stackBuilder.addNextIntent(resultIntent);
	PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
		PendingIntent.FLAG_UPDATE_CURRENT);
	mBuilder.setContentIntent(resultPendingIntent);
	NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	// mId allows you to update the notification later on.
	mNotificationManager.notify(2, mBuilder.build());

	// Issue the notification here.
    }

    private void notificationSimple() {
	NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(
		this).setSmallIcon(android.R.drawable.ic_dialog_email)
		.setContentTitle("My notification")
		.setContentText("Hello World!")
		.setVibrate(new long[] { 1000, 2000 }).setAutoCancel(true)
		.setPriority(0);
	// Creates an explicit intent for an Activity in your app
	Intent resultIntent = new Intent(this, NotificationActivity2.class);

	// The stack builder object will contain an artificial back stack for
	// the
	// started Activity.
	// This ensures that navigating backward from the Activity leads out of
	// your application to the Home screen.
	TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
	// Adds the back stack for the Intent (but not the Intent itself)
	stackBuilder.addParentStack(NotificationActivity.class);
	// Adds the Intent that starts the Activity to the top of the stack
	stackBuilder.addNextIntent(resultIntent);
	PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
		PendingIntent.FLAG_UPDATE_CURRENT);
	mBuilder.setContentIntent(resultPendingIntent);
	NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
	// mId allows you to update the notification later on.
	mNotificationManager.notify(1, mBuilder.build());
    }

}
