package com.android.test.multithreaddownloadfile.network;

public interface DownloadProgressListener {
	public void onDownloadSize(int size);
}
