package com.android.test.multithreaddownloadfile.network;

import java.io.File;

import android.app.Activity;
import android.os.AsyncTask;
import android.widget.ProgressBar;
import android.widget.TextView;

public class FileDownloaderAsyncTask extends AsyncTask<String, Integer, Integer> {

	private Activity mActivity ;
	private ProgressBar progress ;
	private TextView progressText ;
	
	/**
	 * 
	 * @param activity			
	 * @param progress			进度条
	 * @param progressText		为了提示现在百分比
	 */
	public FileDownloaderAsyncTask(Activity activity, ProgressBar progress, TextView progressText){
		this.mActivity = activity ;
		this.progress = progress ;
		this.progressText = progressText ;
	}
	
	/**
	 * arg0[0]: URL path
	 * arg0[1]: path to save file 
	 */
	@Override
	protected Integer doInBackground(String... arg0) {
		String[] args = arg0 ;
		String url = args[0] ;
		String savedir = args[1] ;
		FileDownloader loader = new FileDownloader(mActivity, url, new File(savedir), 5);
		progress.setMax(loader.getFileSize()) ;
		int downloadSize  = 0;
		try {
			downloadSize = loader.download(new DownloadProgressListener() {
				
				@Override
				public void onDownloadSize(int size) {
					publishProgress(size) ;
				}
			}) ;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return downloadSize;
	}
	
	
	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		progress.setProgress(values[0]) ;
		float num = (float)values[0]/(float)progress.getMax();
		int result = (int)(num*100);
		progressText.setText(result+ "%");
	}
	
	@Override
	protected void onPostExecute(Integer result) {
		super.onPostExecute(result);
		if(result == progress.getMax()){
			//下载完成
		}
	}
	

}
