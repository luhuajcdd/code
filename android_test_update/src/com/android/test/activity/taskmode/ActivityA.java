package com.android.test.activity.taskmode;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.android.test.R;

public class ActivityA extends Activity {

    private TextView tv ; 
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_lunch_mode_test) ;
        tv = (TextView)findViewById(R.id.mode) ;
        setTextForTextView("A "+this.getTaskId() + "");
        
        findViewById(R.id.go).setOnClickListener(new OnClickListener() {
	    
	    @Override
	    public void onClick(View v) {
		Intent intent = new Intent("android.intent.action.B") ;
//		intent.setFlags(Intent.FLAG_ACTIVITY_TASK_ON_HOME) ;
		startActivity(intent) ;
	    }
        }) ;
        
    }

    private void setTextForTextView(String text) {
	System.out.println(text);
	tv.setText(text) ;
    }
    

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        System.out.println("A " + "onNewIntent");
    }
    
    
}
