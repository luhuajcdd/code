package com.android.test.activity.taskmode;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.android.test.R;

public class ActivityD extends Activity {

    private TextView tv ; 
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_lunch_mode_test) ;
        tv = (TextView)findViewById(R.id.mode) ;
        setTextForTextView("D "+this.getTaskId() + "");
        
        findViewById(R.id.go).setOnClickListener(new OnClickListener() {
	    @Override
	    public void onClick(View v) {
		startActivity(new Intent("android.intent.action.E")) ;
	    }
        }) ;
        
    }

    private void setTextForTextView(String text) {
	System.out.println(text);
	tv.setText(text) ;
    }
    

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        System.out.println("D " + "onNewIntent");
    }
    
}
