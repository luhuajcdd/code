package com.android.test.activity.configuration;

import com.android.test.R;

import android.app.Activity;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class OriginalKeyboardHiddenScreenSize extends Activity {

    private TextView textView ;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.layout_original_keyboardhidden_screensize) ;
        textView = (TextView)findViewById(R.id.tv);
        
        setText("onCreate()") ;
        
    }
    
    @Override
    protected void onStart() {
        super.onStart();
        setText("onStart()") ;
    }
    
    @Override
    protected void onRestart() {
        super.onRestart();
        setText("onRestart()") ;
    }
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        setText("onRestoreInstanceState()") ;
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        setText("onResume()") ;
    }
    
    @Override
    protected void onPause() {
        super.onPause();
        System.out.println("onPause()");
        setText("onPause()") ;
    }
    
    //before onStop(if called)
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        System.out.println("onSaveInstanceState()");
        setText("onSaveInstanceState()") ;
    }
    
    @Override
    protected void onStop() {
        super.onStop();
        System.out.println("onStop()");
        setText("onStop()") ;
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.out.println("onDestroy()");
        setText("onDestroy()") ;
    }
    
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        System.out.println("onConfigurationChanged()");
        setText("onConfigurationChanged()") ;
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
            System.out.println("landscape");
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
            System.out.println("portrait");
        }
        
        if (newConfig.keyboardHidden == Configuration.KEYBOARDHIDDEN_NO){
            Toast.makeText(this, "keyboardhidden_no", Toast.LENGTH_SHORT).show();
            System.out.println("keyboardhidden_no");
        } else if (newConfig.keyboardHidden == Configuration.KEYBOARDHIDDEN_YES){
            Toast.makeText(this, "keyboardhidden_yes", Toast.LENGTH_SHORT).show();
            System.out.println("keyboardhidden_yes");
        } else if (newConfig.keyboard == Configuration.KEYBOARDHIDDEN_UNDEFINED){
            Toast.makeText(this, "keyboardhidden_undefined", Toast.LENGTH_SHORT).show();
            System.out.println("keyboardhidden_undefined");
        }
        
        if (newConfig.screenHeightDp == Configuration.SCREEN_HEIGHT_DP_UNDEFINED){
            Toast.makeText(this, "screen_height_dp_undefined", Toast.LENGTH_SHORT).show();
            System.out.println("screen_height_dp_undefined");
        } else if (newConfig.screenWidthDp == Configuration.SCREEN_WIDTH_DP_UNDEFINED){
            Toast.makeText(this, "screen_width_dp_undefined", Toast.LENGTH_SHORT).show();
            System.out.println("screen_width_dp_undefined");
        } else if (newConfig.screenLayout == Configuration.SCREENLAYOUT_SIZE_NORMAL){
            Toast.makeText(this, "screenlayout_size_normal", Toast.LENGTH_SHORT).show();
            System.out.println("screenlayout_size_normal");
        } else if (newConfig.screenLayout == Configuration.SCREENLAYOUT_SIZE_LARGE){
            Toast.makeText(this, "screenlayout_size_large", Toast.LENGTH_SHORT).show();
            System.out.println("screenlayout_size_large");
        } else if (newConfig.screenLayout == Configuration.SCREENLAYOUT_SIZE_SMALL){
            Toast.makeText(this, "screenlayout_size_small", Toast.LENGTH_SHORT).show();
            System.out.println("screenlayout_size_small");
        }
    }
    
    public Object onRetainNonConfigurationInstance() {
	System.out.println("onRetainNonConfigurationInstance()");
	setText("onRetainNonConfigurationInstance()") ;
	return null;
    }
    
    @Override
    public Object getLastNonConfigurationInstance() {
	System.out.println("getLastNonConfigurationInstance()");
	setText("getLastNonConfigurationInstance()") ;
        return super.getLastNonConfigurationInstance();
    }
    
    private void setText(String str){
	textView.setText(textView.getText() + "\r\n" + str) ;
    }
}
