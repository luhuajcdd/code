package com.android.test.service.remote;

import com.android.test.service.remote.Book;

interface IRemoteService{
	
	int getPID() ;
	
	void basicInt(int i) ;
	
	void basicByte(byte b) ;
	
	void basicLong(long l) ;
	
	void basicDouble(double d) ;
	
	void basicFloat(float f) ;
	
	void basicString(String s) ;
	
	void basicBoolean(boolean b) ;
	
	void basicCharSequence(char c) ;
	
	void basicList(inout List<String> l) ;
	
	void basicMap(in Map m) ;
	
	Book getBook() ;
	
}