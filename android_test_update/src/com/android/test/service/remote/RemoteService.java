package com.android.test.service.remote;

import java.util.List;
import java.util.Map;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.os.Process;
import android.os.RemoteException;

public class RemoteService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		return iBinder;
	}

	private final IRemoteService.Stub iBinder = new IRemoteService.Stub() {
		
		@Override
		public int getPID() throws RemoteException {
			return  Process.myPid();
		}
		
		@Override
		public Book getBook() throws RemoteException {
			Book book = new Book() ;
			book.setName("name") ;
			return book;
		}
		
		@Override
		public void basicString(String s) throws RemoteException {
			System.out.println("string = "+s);
			
		}
		
		@Override
		public void basicMap(Map m) throws RemoteException {
			System.out.println("Map size = "+m.size());
			
		}
		
		@Override
		public void basicLong(long l) throws RemoteException {
			
			System.out.println("long = "+l);
		}
		
		@Override
		public void basicList(List<String> l) throws RemoteException {
			System.out.println("List size = "+l.size());
			
		}
		
		@Override
		public void basicInt(int i) throws RemoteException {
			
			System.out.println("int = "+i);
		}
		
		@Override
		public void basicFloat(float f) throws RemoteException {
			
			System.out.println("float = "+f);
		}
		
		@Override
		public void basicDouble(double d) throws RemoteException {
			
			System.out.println("double = "+d);
		}
		
		@Override
		public void basicCharSequence(char c) throws RemoteException {
			System.out.println("char = "+c);
			
		}
		
		@Override
		public void basicByte(byte b) throws RemoteException {
			
			
		}
		
		@Override
		public void basicBoolean(boolean b) throws RemoteException {
			
			
		}
	};

	@Override
	public boolean onUnbind(Intent intent) {
	    System.out.println("onUnbind()");
	    return super.onUnbind(intent) ;
	};
}
