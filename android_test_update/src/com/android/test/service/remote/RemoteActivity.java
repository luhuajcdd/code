package com.android.test.service.remote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.android.test.R;

public class RemoteActivity extends Activity {

	private IRemoteService  remoteService ;

	private boolean bindFlag = false ;
	private TextView tv  ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_service_remote) ;
		findViewById(R.id.remote).setOnClickListener(l) ;
		tv = (TextView)findViewById(R.id.textView1) ;
		
	}
	
	private OnClickListener l = new OnClickListener(){

		@Override
		public void onClick(View v) {
			if(remoteService != null){
				try {
					remoteService.basicByte((byte)4) ;
					remoteService.basicBoolean(true) ;
					remoteService.basicInt(132) ;
					remoteService.basicLong(146) ;
					remoteService.basicFloat(83646.3f) ;
					remoteService.basicDouble(2.12) ;
					remoteService.basicString("remoteService") ;
					remoteService.basicCharSequence('r') ;
					List<String> l = new ArrayList<String>() ;
					l.add("Remote") ;
					l.add("Service") ;
					remoteService.basicList(l) ;
					Map<String,String> m = new HashMap<String,String>();
					m.put("a", "a") ;
					remoteService.basicMap(m) ;
					Book b = remoteService.getBook() ;
					System.out.println(b.getName());
					tv.setText(b.getName() + "  " );
					tv.setTextColor(RemoteActivity.this.getResources().getColor(android.R.color.holo_green_light));
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				
			}
		}
		
	} ;
	
	@Override
	protected void onStart() {
		super.onStart();
		binderService() ;
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		unBindService() ;
		remoteService = null ;
	}
	
	private ServiceConnection conn = new ServiceConnection(){

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			remoteService = IRemoteService.Stub.asInterface(service) ;
			bindFlag = true ;
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			remoteService = null ;
			bindFlag = false ;
		}
		
	} ;
	
	private void binderService(){
		bindService(new Intent("android.action.service.REMOTESERVICE"), conn, Context.BIND_AUTO_CREATE) ;
	}
	
	private void unBindService(){
		unbindService(conn) ;
		bindFlag = false ;
		conn = null ;
	}
	
}
