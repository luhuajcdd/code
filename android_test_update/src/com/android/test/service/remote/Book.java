package com.android.test.service.remote;

import android.os.Parcel;
import android.os.Parcelable;

public class Book implements Parcelable {

	private String name ;
	
	@Override
	public int describeContents() {
		return 0;
	}
	
	public Book(){}
	
    private Book(Parcel in) {
        readFromParcel(in);
    }

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(name) ;
	}

	
	public void readFromParcel(Parcel in){
		name = in.readString() ;
	}
	
	
	public final static Parcelable.Creator<Book> CREATOR = new Parcelable.Creator<Book>(){

		@Override
		public Book createFromParcel(Parcel source) {
			
			return new Book(source);
		}

		@Override
		public Book[] newArray(int size) {
			
			return new Book[size];
		}
		
	} ;
	
	public void setName(String name){
		this.name = name ;
	}
	
	public String getName(){
		return this.name ;
	}
	
}
