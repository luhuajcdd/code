package com.android.test.service.local;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.android.test.R;

public class CoverActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.layout_service_cover);
		
		new Handler().postDelayed(new Runnable(){
			 
	         @Override
	         public void run() {
	             Intent mainIntent = new Intent("android.intent.action.SERVICE_PLAY");
	             CoverActivity.this.startActivity(mainIntent);
	             CoverActivity.this.finish();
	         }
	           
	        }, 2000);
		
	}
}