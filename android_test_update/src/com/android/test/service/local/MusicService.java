package com.android.test.service.local;

import com.android.test.R;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

public class MusicService extends Service {
	
	String tag = "yao";

	public static MediaPlayer mPlayer;

	public boolean isPause = false;

	IServicePlayer.Stub stub = new IServicePlayer.Stub() {
		
		@Override
		public void play() throws RemoteException {
			mPlayer.start();
		}

		@Override
		public void pause() throws RemoteException {
			mPlayer.pause();
		}

		@Override
		public void stop() throws RemoteException {
			mPlayer.stop();
		}

		@Override
		public int getDuration() throws RemoteException {
			return mPlayer.getDuration();
		}

		@Override
		public int getCurrentPosition() throws RemoteException {
			return mPlayer.getCurrentPosition();
		}

		@Override
		public void seekTo(int current) throws RemoteException {
			mPlayer.seekTo(current);
		}

		@Override
		public boolean setLoop(boolean loop) throws RemoteException {
			return false;
		}

	};

	@Override
	public void onCreate() {
	    System.out.println("onCreate");
		mPlayer = MediaPlayer.create(getApplicationContext(), R.raw.cnhk) ;
		//mPlayer = MediaPlayer.create(getApplicationContext(), ElfPlayerUtil.getFileinSD("cnhk.mp3"));
	}

	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
	    System.out.println("onStartCommand");
	return super.onStartCommand(intent, flags, startId);
	}
	
	@Override
	public void onDestroy() {
	super.onDestroy();
	System.out.println("onDestroy");
	}
	
	@Override
	public IBinder onBind(Intent intent) {
	    System.out.println("onBind()");
	    return stub;
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
	    System.out.println("onUnbind");
	    stub = null;
	return super.onUnbind(intent);
	}

}