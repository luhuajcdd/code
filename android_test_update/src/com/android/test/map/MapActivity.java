package com.android.test.map;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.widget.TextView;

import com.android.test.R;
import com.android.test.location.baidu.LocationPointInfo;
import com.android.test.location.baidu.LocationUtils;
import com.android.test.location.baidu.SangforLocationClient;
import com.android.test.location.baidu.SangforLocationClient.NotifyLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.BMapManager;
import com.baidu.mapapi.MKGeneralListener;
import com.baidu.mapapi.map.LocationData;
import com.baidu.mapapi.map.MapController;
import com.baidu.mapapi.search.MKAddrInfo;
import com.baidu.mapapi.search.MKBusLineResult;
import com.baidu.mapapi.search.MKDrivingRouteResult;
import com.baidu.mapapi.search.MKPoiResult;
import com.baidu.mapapi.search.MKSearch;
import com.baidu.mapapi.search.MKSearchListener;
import com.baidu.mapapi.search.MKShareUrlResult;
import com.baidu.mapapi.search.MKSuggestionResult;
import com.baidu.mapapi.search.MKTransitRouteResult;
import com.baidu.mapapi.search.MKWalkingRouteResult;
import com.baidu.platform.comapi.basestruct.GeoPoint;

public class MapActivity extends Activity implements OnClickListener{

	private final String MAP_KEY = "D9bbe7db7424409b40569e6a99b5053b"; 
	
	private BMapManager mBMapMan = null;  
	private BaiduMapView mMapView = null;  
	private MapController mMapController = null;
	
    private SangforLocationClient mSangforLocationClient ;
	private LocationClient mLocClient ;
	private LocationData mLocData ;
	private static GeoPoint mGeoPoint ;
	private static MKSearch mkSerach;
	
	private TextView mAddress ;
	//定位图层
	private LocationOverlay myLocationOverlay = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);  
		
		mBMapMan=new BMapManager(getApplication());  
		mBMapMan.init(MAP_KEY,sangforGeneralListener ); 
		
		//注意：请在试用setContentView前初始化BMapManager对象，否则会报错  
		setContentView(R.layout.layout_map);  
		
		View view = this.getLayoutInflater().inflate(R.layout.layout_location_tip, null) ;
		getWindow().addContentView(
				view,
				new LayoutParams(LayoutParams.MATCH_PARENT,
						LayoutParams.MATCH_PARENT));
		
		mAddress = (TextView)view.findViewById(R.id.address);
		
		init();
		
		GeoPoint p ;
        double cLat = 22.550562 ;
        double cLon = 113.952486;
        Intent  intent = getIntent();
        if ( intent.hasExtra("x") && intent.hasExtra("y") ){
        	//当用intent参数时，设置中心点为指定点
        	Bundle b = intent.getExtras();
        	p = new GeoPoint(b.getInt("y"), b.getInt("x"));
        }else{
        	//设置中心点为天安门
        	 p = new GeoPoint((int)(cLat * 1E6), (int)(cLon * 1E6));
        }
		
		mMapView=(BaiduMapView)findViewById(R.id.bmapsView);  
		mMapView.setBuiltInZoomControls(true);  
		//设置启用内置的缩放控件  
		mMapController=mMapView.getController();  
//		mMapController.enableClick(true) ;
		// 得到mMapView的控制权,可以用它控制和驱动平移和缩放  
		//用给定的经纬度构造一个GeoPoint，单位是微度 (度 * 1E6)  
		mMapController.setCenter(p);//设置地图中心点  
		mMapController.setZoom(16);//设置地图zoom级别 
		
		mkSerach = new MKSearch();
		mkSerach.init(mBMapMan, new MKSearchListener() {

			@Override
			public void onGetWalkingRouteResult(MKWalkingRouteResult arg0,
					int arg1) {
			}

			@Override
			public void onGetTransitRouteResult(MKTransitRouteResult arg0,
					int arg1) {
			}

			@Override
			public void onGetSuggestionResult(MKSuggestionResult arg0, int arg1) {
			}

			@Override
			public void onGetPoiResult(MKPoiResult arg0, int arg1, int arg2) {
			}

			@Override
			public void onGetPoiDetailSearchResult(int arg0, int arg1) {
			}

			@Override
			public void onGetDrivingRouteResult(MKDrivingRouteResult arg0,
					int arg1) {
			}

			@Override
			public void onGetBusDetailResult(MKBusLineResult arg0, int arg1) {
			}

			@Override
			public void onGetAddrResult(MKAddrInfo info, int arg1) {
				mAddress.setText(info.strAddr) ;
			}

			@Override
			public void onGetShareUrlResult(MKShareUrlResult arg0, int arg1, int arg2) {
			}
		});
	
		//定位图层初始化
		myLocationOverlay = new LocationOverlay(this,mMapView);
		mLocData = new LocationData();
		//设置定位数据
	    myLocationOverlay.setData(mLocData);
	    //添加定位图层
		mMapView.getOverlays().add(myLocationOverlay);
		myLocationOverlay.enableCompass();
		//修改定位数据后刷新图层生成
		mMapView.refresh();
	}
	
	private void init() {
		mSangforLocationClient = SangforLocationClient.newInstance(this) ;
		mSangforLocationClient.setNotifyLocationListener(notifyListener);
		mLocClient = mSangforLocationClient.getClient();
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(false);
		option.setAddrType("all");//返回的定位结果包含地址信息
		option.setCoorType("bd09ll");//返回的定位结果是百度经纬度,默认值gcj02
		option.setScanSpan(300000);//设置发起定位请求的间隔时间为1000ms
		option.disableCache(true);//禁止启用缓存定位
		option.setPoiNumber(5);	//最多返回POI个数	
		option.setPoiDistance(500); //poi查询距离		
		option.setPoiExtraInfo(true); //是否需要POI的电话和地址等详细信息		
		//基站精度为平均100~300米，视运营商基站覆盖范围而定。WIFI精度为30~200米。
		//GPS精度最高，为30米左右。在户外，先开启GPS再进行定位，结果较准。
		//但GPS比较费电，且在室内不可用。
		if(LocationUtils.isGPSOpen(this)){
			option.setPriority(LocationClientOption.GpsFirst) ;
		}else{
			option.setPriority(LocationClientOption.NetWorkFirst) ;
		}
		option.setProdName("SANGFOR_Office") ;
		mLocClient.setLocOption(option);
		
	}
	
   /**
     * 修改位置图标
     * @param marker
     */
    public void modifyLocationOverlayIcon(Drawable marker){
    	//当传入marker为null时，使用默认图标绘制
    	myLocationOverlay.setMarker(marker);
    	//修改图层，需要刷新MapView生效
    	mMapView.refresh();
    }
	
	private MKGeneralListener sangforGeneralListener = new MKGeneralListener(){

		@Override
		public void onGetNetworkState(int arg0) {
			
		}

		@Override
		public void onGetPermissionState(int arg0) {
			
		}
	};
	
	private NotifyLocationListener notifyListener = new NotifyLocationListener() {
		
		@Override
		public void notify(LocationPointInfo locationPointInfo) {
			mLocData.latitude = locationPointInfo.getLatitude();
            mLocData.longitude = locationPointInfo.getLongitude();
            //如果不显示定位精度圈，将accuracy赋值为0即可
            mLocData.accuracy = locationPointInfo.getRadius();
            mLocData.direction = locationPointInfo.getDerect();
            System.out.println( locationPointInfo.getLatitude()* 1e6 + "  " + locationPointInfo.getLongitude()* 1e6);
            //更新定位数据
            myLocationOverlay.setData(mLocData);
            //更新图层数据执行刷新后生效
            mMapView.refresh();
            
            mAddress.setText(locationPointInfo.getAddrStr()) ;
            
            mMapController.animateTo(new GeoPoint((int)(mLocData.latitude* 1e6), (int)(mLocData.longitude *  1e6)));
		}
	};
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		}
	}
	
	public LocationData getLocData() {
		return mLocData;
	}

	public static GeoPoint getGeoPoint() {
		return mGeoPoint;
	}

	public static void getPosition(GeoPoint g) {
		System.out.println( g.getLatitudeE6() + "  " + g.getLongitudeE6());
		mkSerach.reverseGeocode(g);
		mGeoPoint = g ;
	}
	
	@Override  
	protected void onDestroy(){ 
		super.onDestroy();  
		if(mMapView != null){
			mMapView.destroy();  
		}
        if(mBMapMan!=null){  
                mBMapMan.destroy();  
                mBMapMan=null;  
        }  
	}  
	
	@Override
	protected void onStop() {
		super.onStop();
		if(mBMapMan!=null){  
             mBMapMan.stop();  
             mBMapMan=null;  
		}  
	}
	
	@Override  
	protected void onPause(){
		super.onPause();   
		if(mMapView != null){
	        mMapView.onPause();
		}
		mSangforLocationClient.unRegisterLocationListener() ;
	}  
	@Override  
	protected void onResume(){  
		super.onResume();
		if(mMapView != null){
	        mMapView.onResume();  
		}
        if(mBMapMan!=null){  
                mBMapMan.start();  
        }  
        if(!mLocClient.isStarted()){
			mLocClient.start();
			mSangforLocationClient.registerLocationListener();
		}
	    mLocClient.requestLocation() ;
	}
	
	@Override
	protected void onRestart() {
		super.onRestart();
	}
	
	@Override
	protected void onStart() {
		super.onStart();
	}
}
