package com.android.test.map;

import com.baidu.mapapi.map.MapView;
import com.baidu.mapapi.map.MyLocationOverlay;

//继承MyLocationOverlay重写dispatchTap实现点击处理
public class LocationOverlay extends MyLocationOverlay {

	public LocationOverlay(MapActivity activity, MapView mapView) {
		super(mapView);
	}

	@Override
	protected boolean dispatchTap() {
		return true;
	}

}