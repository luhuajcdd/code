package com.android.test.map;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.baidu.mapapi.map.MapView;
import com.baidu.platform.comapi.basestruct.GeoPoint;

public class BaiduMapView extends MapView {
	
	private CenterView centerView ;
	
	public BaiduMapView(Context arg0){
		super(arg0);
		centerView = new CenterView(arg0) ;
	}
	
	public BaiduMapView(Context arg0, AttributeSet arg1) {
		super(arg0, arg1);
		centerView = new CenterView(arg0) ;
	}
	
	public BaiduMapView(Context context, AttributeSet attrs, int defStyle){
		super(context, attrs, defStyle);
		centerView = new CenterView(context) ;
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if(centerView == null || centerView.w == 0 || centerView.h == 0 ){
			centerView = new CenterView(this.getContext()) ;
		}
		GeoPoint g = this.getProjection().fromPixels(centerView.w, centerView.h);
		if(g != null){
			MapActivity.getPosition(g);
		}
		
		return super.onTouchEvent(event);
	}
}
