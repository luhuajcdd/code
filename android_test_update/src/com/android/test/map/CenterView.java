package com.android.test.map;




import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.View;

import com.android.test.R;

public class CenterView extends View{
	public int w;
	public int h;

	public  Bitmap mBitmap;

	public CenterView(Context context) {
		super(context);
		mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.me);
		DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
		w = displayMetrics.widthPixels / 2 - mBitmap.getWidth() / 2 - 10;
		h = displayMetrics.heightPixels / 2 - mBitmap.getHeight() / 2 - 60;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		w = this.getWidth() / 2 - mBitmap.getWidth() / 2;
		h = this.getHeight() / 2 - mBitmap.getHeight() / 2;
		canvas.drawBitmap(mBitmap, w, h, null);
	}
	
}
