package com.android.test.location.baidu;

import java.util.List;

import android.content.Context;

import com.android.test.location.baidu.LocationPointInfo.POI;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;

/**
 * 定位帮助类
 * @author luhua
 *
 */
public class SangforLocationClient {
	
	private static SangforLocationClient mSangforLocationClient ;
	private LocationClient mClient ;
	
	private SangforLocationListener mLocatListener = new SangforLocationListener() ;
	
	//通知监听器，当获取location时，通知监听者
	private NotifyLocationListener mNotifyLocationListener ;
	
	private SangforLocationClient(Context context){
		//初始化LocationClient 
		mClient = new LocationClient(context.getApplicationContext()) ;
		//注册location 监听器
		mClient.registerLocationListener(mLocatListener) ;
	}
	
	public static SangforLocationClient newInstance(Context context){
		if(mSangforLocationClient == null){
			mSangforLocationClient = new SangforLocationClient(context) ;
		}
		return mSangforLocationClient;
	}
	
	public LocationClient getClient() {
		return mClient;
	}

	public void setNotifyLocationListener(NotifyLocationListener notifyLocationListener) {
		this.mNotifyLocationListener = notifyLocationListener;
	}

	/**
	 * 移除监听事件
	 */
	public void unRegisterLocationListener(){
		mClient.unRegisterLocationListener(mLocatListener) ;
	}
	
	public void registerLocationListener(){
		mClient.registerLocationListener(mLocatListener) ;
	}
	
	public class SangforLocationListener implements BDLocationListener{
		
		@Override
		public void onReceiveLocation(BDLocation location) {
			
			if(location == null){
				return ;
			}
			boolean result = checkLocationType(location);
			if(!result){
				return ;
			}
			
			getLocationInfo(location);
		}

		@Override
		public void onReceivePoi(BDLocation location) {

			if(location == null){
				return ;
			}
			boolean result = checkLocationType(location);
			if(!result){
				return ;
			}
			
			getLocationInfo(location);
			
		}
		

		private boolean checkLocationType(BDLocation location) {
			//61 ： GPS定位结果
			//62 ： 扫描整合定位依据失败。此时定位结果无效。
			//63 ： 网络异常，没有成功向服务器发起请求。此时定位结果无效。
			//65 ： 定位缓存的结果。
			//66 ： 离线定位结果。通过requestOfflineLocaiton调用时对应的返回结果
			//67 ： 离线定位失败。通过requestOfflineLocaiton调用时对应的返回结果
			//68 ： 网络连接失败时，查找本地离线定位时对应的返回结果
			//161： 表示网络定位结果
			//162~167： 服务端定位失败。
			int type = location.getLocType() ;
			if(type == BDLocation.TypeCriteriaException
					|| type == BDLocation.TypeNetWorkException 
					|| type == BDLocation.TypeOffLineLocationFail 
					|| type == BDLocation.TypeOffLineLocationNetworkFail
					|| type > BDLocation.TypeNetWorkLocation){
				return false;
			}
			return true ;
		}

		@SuppressWarnings("unchecked")
		public void getLocationInfo(BDLocation location) {
			
			LocationPointInfo info = new LocationPointInfo() ;
			info.latitude = location.getLatitude() ;
			info.longitude = location.getLongitude() ;
			info.derect  = location.getDerect() ;
			if(location.hasRadius()){
				info.radius = location.getRadius() ;
			}
			if(location.getLocType() == BDLocation.TypeNetWorkLocation){
				info.addrStr = location.getAddrStr() ;
			}
			info.province = location.getProvince() ;
			info.city = location.getCity() ;
			info.district = location.getDistrict() ;
			if(location.hasPoi()){
				String poi = location.getPoi() ;
				JsonParser parser = new JsonParser();
				JsonElement element = parser.parse(poi) ;
				Gson gson = new Gson();
				info.poi = (List<POI>) gson.fromJson(element.getAsJsonObject().get("p"),
				 			new TypeToken<List<POI>>(){}.getType());
			}
			if (location.getLocType() == BDLocation.TypeGpsLocation){
				info.speed = location.getSpeed() ;
				info.satelliteNumber = location.getSatelliteNumber() ;
			}
			
			//通知结果到监听事件
			if(null != mNotifyLocationListener){
				mNotifyLocationListener.notify(info) ;
			}
			
		}

	}
	
	public interface NotifyLocationListener{
		
		public void notify(LocationPointInfo locationPointInfo);
		
	}
	
}
