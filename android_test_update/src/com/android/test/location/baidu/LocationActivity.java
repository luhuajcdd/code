package com.android.test.location.baidu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.android.test.R;
import com.android.test.location.baidu.SangforLocationClient.NotifyLocationListener;
import com.android.test.map.MapActivity;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.mapapi.map.LocationData;

public class LocationActivity extends Activity implements OnClickListener {

	private LocationClient mLocClient ;
	private SangforLocationClient sangforLocationClient ;
	private TextView location ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_location);
		initView() ;
		
		init() ;
		
	}

	private void initView() {
		findViewById(R.id.loc).setOnClickListener(this) ;
		findViewById(R.id.poi).setOnClickListener(this) ;
		findViewById(R.id.offline).setOnClickListener(this) ;		
		location = (TextView)findViewById(R.id.location);
	}

	/**
	 * 
	 */
	private void init() {
		sangforLocationClient = SangforLocationClient.newInstance(this) ;
		mLocClient =  sangforLocationClient.getClient();
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(false);
		option.setAddrType("all");//返回的定位结果包含地址信息
		option.setCoorType("bd09ll");//返回的定位结果是百度经纬度,默认值gcj02
		option.setScanSpan(1000);//设置发起定位请求的间隔时间为1000ms
		option.disableCache(true);//禁止启用缓存定位
		option.setPoiNumber(5);	//最多返回POI个数	
		option.setPoiDistance(500); //poi查询距离		
		option.setPoiExtraInfo(true); //是否需要POI的电话和地址等详细信息		
		//基站精度为平均100~300米，视运营商基站覆盖范围而定。WIFI精度为30~200米。
		//GPS精度最高，为30米左右。在户外，先开启GPS再进行定位，结果较准。
		//但GPS比较费电，且在室内不可用。
		if(LocationUtils.isGPSOpen(this)){
			option.setPriority(LocationClientOption.GpsFirst) ;
		}else{
			option.setPriority(LocationClientOption.NetWorkFirst) ;
		}
		option.setProdName("SANGFOR_MOA") ;
		mLocClient.setLocOption(option);
		
		mLocClient.start();
	}

	
	@Override
	public void onClick(View v) {
		sangforLocationClient.setNotifyLocationListener(notifyListener);
		mLocClient.start();
		switch (v.getId()) {
		case R.id.loc:
			if (mLocClient != null && mLocClient.isStarted())
				mLocClient.requestLocation();
			else 
				Log.d("LocSDK3", "locClient is null or not started");
			break;
		case R.id.poi:
			if (mLocClient != null && mLocClient.isStarted())
				mLocClient.requestPoi();
			break;
		case R.id.offline:
			if (mLocClient != null && mLocClient.isStarted())
				mLocClient.requestOfflineLocation();
			break;
		}
	}

	private NotifyLocationListener notifyListener = new NotifyLocationListener() {
		
		@Override
		public void notify(LocationPointInfo locationPointInfo) {
			LocationData locData = new LocationData();
			locData.latitude = locationPointInfo.getLatitude();
            locData.longitude = locationPointInfo.getLongitude();
            //如果不显示定位精度圈，将accuracy赋值为0即可
            locData.accuracy = locationPointInfo.getRadius();
            locData.direction = locationPointInfo.getDerect();
            System.out.println( locationPointInfo.getLatitude()* 1e6 + "  " + locationPointInfo.getLongitude()* 1e6);
            location.setText("维度：="+locationPointInfo.getLatitude()* 1e6 + " " +
            		"\n 经度:= " + locationPointInfo.getLongitude()* 1e6  + 
            		"\n 地址:= " + locationPointInfo.addrStr) ;
            mLocClient.stop();
		}
	};
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		
		//结束定位的后台进程
		mLocClient.stop() ;
	}
	
}
