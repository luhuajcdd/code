package com.android.test.location.baidu;

import java.util.List;

public class LocationPointInfo{
	
	//维度
	public double latitude ;
	//经度
	public double longitude ;
	//定位精度半径,单位是米
	public float radius ;
	//文字描述的地址
	public String addrStr ;
	//获取省份信息
	public String province ;
	//获取城市信息
	public String city ;
	//获取区县信息
	public String district ;
	//获取poi信息
	public List<POI> poi ;
	//GPS定位速度
	public float speed ;
	//GPS定位卫星
	public int satelliteNumber ;
	//方向
	public float derect;
//	public double getLatitude ( )     //获取维度
//	public double getLongitude ( )    //获取经度
//	public boolean hasRadius ( )    //判断是否有定位精度半径
//	public float getRadius ( )    //获取定位精度半径，单位是米
//	public String getAddrStr ( )     //获取反地理编码
//	public String getProvince ( )     //获取省份信息
//	public String getCity ( )     //获取城市信息
//	public String getDistrict ( )     //获取区县信息
//	public boolean hasPoi()
//	public String getPoi()     //获取poi信息
//	{"p":
//	  [
//		{"addr":"xx路xx号"，"y":"40.2234","dis":"324.3","x":"116.3424","name":"xx酒店","tel":""},
//		{                 "y":"40.2234","dis":"324.3","x":"116.3424","name":"xx酒店"}
//	  ]
//	}
	
	
	public class POI{
		public String addr ;
		public double y ;
		public double x ;
		public String name ;
		public String tel ;
		public String dis ;
		public String getAddr() {
			return addr;
		}
		public void setAddr(String addr) {
			this.addr = addr;
		}
		public double getY() {
			return y;
		}
		public void setY(double y) {
			this.y = y;
		}
		public double getX() {
			return x;
		}
		public void setX(double x) {
			this.x = x;
		}
		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getTel() {
			return tel;
		}
		public void setTel(String tel) {
			this.tel = tel;
		}
		public String getDis() {
			return dis;
		}
		public void setDis(String dis) {
			this.dis = dis;
		}
		
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
	}

	public String getAddrStr() {
		return addrStr;
	}

	public void setAddrStr(String addrStr) {
		this.addrStr = addrStr;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public List<POI> getPoi() {
		return poi;
	}

	public void setPoi(List<POI> poi) {
		this.poi = poi;
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public int getSatelliteNumber() {
		return satelliteNumber;
	}

	public void setSatelliteNumber(int satelliteNumber) {
		this.satelliteNumber = satelliteNumber;
	}

	public float getDerect() {
		return derect;
	}

	public void setDerect(float derect) {
		this.derect = derect;
	}
}
