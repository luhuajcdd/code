package com.android.test.permission;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;

public class ActivityPermission extends Activity{
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Button button = new Button(this) ;
		button.setText("need <uses-permission android:name=\"com.android.test.permission.ACTIVITY_PERMISSION\"/>" +
				" in manifest, otherwise will get permission denial error. but in the same application(needn't that)") ;
		setContentView(button) ;
	}
}
