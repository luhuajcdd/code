package com.android.test.signgesture;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.android.test.R;

public class SignGestureTest extends Activity {
    
    private ImageView iv ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_sign_gesture_test) ;
        
        findViewById(R.id.forward).setOnClickListener(new View.OnClickListener(){

	    @Override
	    public void onClick(View v) {
		//intent to sign activity
		startActivityForResult(new Intent("android.intent.action.GESTURE_ACTIVITY"), 1);
	    }
            
        }) ;
        
        iv = (ImageView)findViewById(R.id.sign_image) ;
        
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
		Bundle bundle = data.getBundleExtra(GestureActivity.DATA);
		Bitmap bitmap = (Bitmap) bundle
				.getParcelable(GestureActivity.SIGN);
		iv.setImageBitmap(bitmap);
        }
    }
}
