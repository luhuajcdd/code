package com.android.test.signgesture;

import com.android.test.R;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.gesture.Gesture;
import android.gesture.GestureOverlayView;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class GestureActivity extends Activity implements OnClickListener {

    public static final String SIGN = "sign" ;
    public static final String DATA = "data" ;
    private static final float LENGTH_THRESHOLD = 120.0f;
    private Button save, cancel;
    private GestureOverlayView overlay;
    private Gesture mGesture;
    private int mThumbnailSize, mThumbnailInset, mPathColor;
    private Bitmap bitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	// TODO Auto-generated method stub
	super.onCreate(savedInstanceState);
	this.setContentView(R.layout.layout_sign_gesture);
	initViews();
	setLinstenerForViews();
    }

    private void initViews() {
	save = (Button) this.findViewById(R.id.bottom_left_button);
	cancel = (Button) this.findViewById(R.id.bottom_right_button);
	overlay = (GestureOverlayView) findViewById(R.id.gestures_overlay);
	overlay.addOnGestureListener(new GesturesProcessor());
	final Resources resources = getResources();
	mPathColor = resources.getColor(R.color.gesture_color);
	mThumbnailInset = (int) resources
		.getDimension(R.dimen.gesture_thumbnail_inset);
	mThumbnailSize = (int) resources
		.getDimension(R.dimen.gesture_thumbnail_size);
    }

    private void setLinstenerForViews() {
	save.setOnClickListener(this);
	cancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
	switch (v.getId()) {
	case R.id.bottom_left_button:
	    saveSign();
	    break;

	case R.id.bottom_right_button:
	    this.finish();
	    break;

	}
    }

    private void saveSign() {
	bitmap = mGesture.toBitmap(mThumbnailSize, mThumbnailSize,
		mThumbnailInset, mPathColor);
	Intent intent = new Intent();
	Bundle data = new Bundle();
	data.putParcelable(SIGN, bitmap);
	intent.putExtra(DATA, data);
	setResult(RESULT_OK, intent);
	this.finish();
    }

    /**
     * 
     * @author hlu
     * 
     */
    private class GesturesProcessor implements
	    GestureOverlayView.OnGestureListener {

	public void onGestureStarted(GestureOverlayView overlay,
		MotionEvent event) {
	    save.setEnabled(false);
	    mGesture = null;
	}

	public void onGesture(GestureOverlayView overlay, MotionEvent event) {
	}

	public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {
	    mGesture = overlay.getGesture();
	    if (mGesture.getLength() < LENGTH_THRESHOLD) {
		overlay.clear(false);
	    }
	    save.setEnabled(true);
	}

	public void onGestureCancelled(GestureOverlayView overlay,
		MotionEvent event) {
	}

    }

    public Bitmap getBitmap() {
	return bitmap;
    }

}
