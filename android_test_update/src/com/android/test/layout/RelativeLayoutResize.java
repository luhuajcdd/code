package com.android.test.layout;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class RelativeLayoutResize extends RelativeLayout {

	private OnRelativeLayoutResizeListener listener ;
	
	public RelativeLayoutResize(Context context) {
		super(context);
	}
	
	public RelativeLayoutResize(Context context, AttributeSet attrs) {
		super(context, attrs) ;
	}
	

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		System.out.println("onMeasure() ---> width = " +  widthMeasureSpec + " , height = " + heightMeasureSpec);
	}
	
	@Override
	protected void onLayout(boolean changed, int l, int t, int r, int b) {
		super.onLayout(changed, l, t, r, b);
		System.out.println("onLayout() -----> changed = " + changed + " , l = " + l + " , t = " + t + " , r = " + r + " , b = " + b );
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		System.out.println("onSizeChanged() ----> w = " + w + " , h = " + h + " , oldW = " + oldw + " , oldH = " + oldh);
		if(listener != null && h < oldh){
			listener.onResize() ;
		}
	}
	
	public void setOnRelativeLayoutResizeListener(OnRelativeLayoutResizeListener listener){
		this.listener = listener ;
	}
	
	//according to set android:windowSoftInputMode="adjustResize" on activity in manifest file
	public interface OnRelativeLayoutResizeListener{
		void onResize() ;
	}
	
}
