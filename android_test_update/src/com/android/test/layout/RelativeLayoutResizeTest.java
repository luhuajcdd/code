package com.android.test.layout;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.android.test.R;
import com.android.test.layout.RelativeLayoutResize.OnRelativeLayoutResizeListener;

public class RelativeLayoutResizeTest extends Activity {
	private TextView text ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_relative_layout_resize);
		text = (TextView)this.findViewById(R.id.text) ;
		RelativeLayoutResize rlr = (RelativeLayoutResize)this.findViewById(R.id.contenter) ;
		rlr.setFocusableInTouchMode(true ) ;
		rlr.setOnRelativeLayoutResizeListener(new OnRelativeLayoutResizeListener() {
			
			@Override
			public void onResize() {
				text.setVisibility(View.GONE) ;
			}
		}) ;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
}
