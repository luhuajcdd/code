package com.android.test.layout;

import com.android.test.R;

import android.app.Activity;
import android.os.Bundle;

public class RelativeLayoutActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_relativelayout) ;
    }
    
}
