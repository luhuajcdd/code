package com.android.test.popup;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.android.test.R;

public class PopupWindowTestActivity extends Activity {

	private Button Left, right, above, below;
	private PopupWindow popupWLeft, popupWRight, popupWAbove, popupWBelow;
	private Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_popup_window);

		mContext = this.getApplicationContext();

		Left = (Button) this.findViewById(R.id.Left);
		right = (Button) this.findViewById(R.id.right);
		above = (Button) this.findViewById(R.id.above);
		below = (Button) this.findViewById(R.id.below);
		
		Left.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (null != popupWLeft)
					if (!popupWLeft.isShowing()) {
						showLeftPopupWindow();
					} else {
						dimissionLeftPopupWindow();
					}
			}
		});
		right.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (null != popupWRight)
					if (!popupWRight.isShowing()) {
						showRightPopupWindow();
					} else {
						dimissionRightPopupWindow();
					}
			}
		});
		below.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (null != popupWBelow)
					if (!popupWBelow.isShowing()) {
						showBelowPopupWindow();
					} else {
						dimissionBelowPopupWindow();
					}
			}
		});
		above.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (null != popupWAbove)
					if (!popupWAbove.isShowing()) {
						showAbovePopupWindow();
					} else {
						dimissionAbovePopupWindow();
					}
			}
		});
		initLeftPopupWindow();
		initRightPopupWindow();
		initAbovePopupWindow();
		initBelowPopupWindow();
	}

	void initLeftPopupWindow() {

		popupWLeft = new PopupWindow(this);
		popupWLeft.setWidth(100);
		popupWLeft.setHeight(200);
		popupWLeft.setAnimationStyle(R.style.AnimationFade);
		popupWLeft.setFocusable(true);
		// 设置为false 界面上除了popup window上面可以点击，其他控件都不可以点击(点击了会无效)， 并且popup
		// window会消失
		// 默认是true. 其他控件都可以点击(点击会产生响应的效果)， 并且popup window会消失
		popupWLeft.setOutsideTouchable(true);
		popupWLeft.setClippingEnabled(true);
		popupWLeft.setIgnoreCheekPress();
		popupWLeft.setInputMethodMode(PopupWindow.INPUT_METHOD_FROM_FOCUSABLE);
		popupWLeft.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.balloon_r_pressed));
		View view = this.getLayoutInflater().inflate(R.layout.layout_popupwindow_view_listview, null);
		popupWLeft.setContentView(view);
		ListView list = (ListView) view.findViewById(R.id.listView1);
		List<String> objects = new ArrayList<String>();
		objects.add("one");
		objects.add("two");
		objects.add("three");
		objects.add("four");
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, objects);
		list.setSelector(R.drawable.task_list_selector);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Toast.makeText(mContext, ((TextView) view).getText().toString(), Toast.LENGTH_SHORT).show();
				// dimissionLeftPopupWindow() ;
			}
		});

	}

	void initRightPopupWindow() {

		popupWRight = new PopupWindow(this);
		popupWRight.setWidth(100);
		popupWRight.setHeight(200);
		popupWRight.setAnimationStyle(R.style.AnimationFade);
		popupWRight.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.balloon_l_pressed));
		popupWRight.setContentView(this.getLayoutInflater().inflate(R.layout.layout_popupwindow_view, null));

	}

	void initAbovePopupWindow() {

		popupWAbove = new PopupWindow(this);
		popupWAbove.setWidth(100);
		popupWAbove.setHeight(200);
		popupWAbove.setAnimationStyle(R.style.AnimationFade);
		// 当输入法弹起来的时候，popupView不会向上移动。
		popupWAbove.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
		popupWAbove.setFocusable(true);
		// 单独设定这句：下面这个两个效果达不到
		// 1. 點擊PopupWindow以外的地方。
		// 2. 點擊左下的Back按鈕。
		// 需要加popupWAbove.setFocusable(true);
		popupWAbove.setBackgroundDrawable(/* new BitmapDrawable()); */this.getResources().getDrawable(R.drawable.billd_selected));
		popupWAbove.setContentView(this.getLayoutInflater().inflate(R.layout.layout_popupwindow_view, null));

	}

	void initBelowPopupWindow() {

		popupWBelow = new PopupWindow(this);
		popupWBelow.setWidth(100);
		popupWBelow.setHeight(200);
		popupWBelow.setAnimationStyle(R.style.AnimationFade);
		popupWBelow.setBackgroundDrawable(this.getResources().getDrawable(R.drawable.near_filter_popup));
		popupWBelow.setContentView(this.getLayoutInflater().inflate(R.layout.layout_popupwindow_view, null));

	}

	void showLeftPopupWindow() {
		popupWLeft.showAtLocation(Left, Gravity.LEFT | Gravity.BOTTOM, 60, 200);
	}

	void dimissionLeftPopupWindow() {
		popupWLeft.dismiss();
	}

	void showRightPopupWindow() {
		popupWRight.showAtLocation(right, Gravity.RIGHT | Gravity.BOTTOM, 50, 140);
	}

	void dimissionRightPopupWindow() {
		popupWRight.dismiss();
	}

	void showAbovePopupWindow() {
		popupWAbove.showAtLocation(above, Gravity.TOP, 50, 185);
	}

	void dimissionAbovePopupWindow() {
		popupWAbove.dismiss();
	}

	void showBelowPopupWindow() {
		popupWBelow.showAsDropDown(below);
	}

	void dimissionBelowPopupWindow() {
		popupWBelow.dismiss();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
