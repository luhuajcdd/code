package com.android.test.contentprovider;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;

public class ContentProviderTestActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ContentValues values = new ContentValues() ;
		values.put("WORD", "abcd") ;
		
		Uri uri = this.getContentResolver().insert(
				Uri.parse("content://com.hualu.contentprovider/mains"),
				values) ;
		String id = uri.getPathSegments().get(1) ;
		
		
		Cursor cAll = this.getContentResolver().query(
				Uri.parse("content://com.hualu.contentprovider/mains"), 
				null, 
				null, 
				null,
				null);
		
		Cursor c = this.getContentResolver().query(
				Uri.parse("content://com.hualu.contentprovider/main/1"), 
				null, 
				null, 
				null,
				null);
		
		
		Toast.makeText(this, "insert success id = " + id + " ," +
				" \r\n All = " + cAll.getCount() + " , " +
				"\r\n one = " + c.getCount(),
				Toast.LENGTH_SHORT).show() ;
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		return true;
	}

}
