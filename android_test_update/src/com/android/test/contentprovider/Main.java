package com.android.test.contentprovider;

public final class Main {

	public static final String AUTHORITY = "com.hualu.contentprovider";
	
	public static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.hualu.main" ;
	
	public static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.hualu.main" ;
	
	/**
     * The scheme part for this provider's URI
     */
    private static final String SCHEME = "content://";
	
	public static final String CONTENT_URI = SCHEME + AUTHORITY ;
	
	
	
}
