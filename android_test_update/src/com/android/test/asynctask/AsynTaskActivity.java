package com.android.test.asynctask;

import java.net.MalformedURLException;
import java.net.URL;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.android.test.R;

public class AsynTaskActivity extends Activity {
    
    private TextView tv ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.layout_asyntask);
	
	findViewById(R.id.click).setOnClickListener(new View.OnClickListener() {
	    @Override
	    public void onClick(View v) {
		DemoAsynTask asyn = new DemoAsynTask(tv);
		try {
		    asyn.execute(new URL("http://www.google.com"));
		} catch (MalformedURLException e) {
		    e.printStackTrace();
		}
	    }
	});
	
	tv = (TextView)findViewById(R.id.result) ;
	
	
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.activity_main, menu);
	return true;
    }

}
