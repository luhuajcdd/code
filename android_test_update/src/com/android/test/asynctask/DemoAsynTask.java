package com.android.test.asynctask;

import java.net.URL;

import android.os.AsyncTask;
import android.widget.TextView;

public class DemoAsynTask extends AsyncTask<URL, String, String> {
    
    private TextView tv ;
    
    public DemoAsynTask(TextView tv){
	this.tv = tv ;
    }

    @Override
    protected String doInBackground(URL... params) {

	publishProgress(params[0].getProtocol() +"://" + params[0].getAuthority() );
	return "9";
    }

    @Override
    protected void onProgressUpdate(String... values) {
	
	StringBuffer sb = new StringBuffer() ;
	sb.append(tv.getText().toString()) ;
	sb.append("\\n\\r") ;
	sb.append(values[0]) ;
	
	tv.setText(sb.toString().replace("\\n", "\n").replace("\\r", "\r")) ;

    }

    @Override
    protected void onPostExecute(String result) {
	System.out.println(result);
    }

}
