package com.android.test.log;

import android.util.Log;

public class LogCore {
	
	public static void logI(String tag, String message){
		Log.i(tag, message) ;
	}
	
	public static void logE(String tag, String message){
		Log.e(tag, message) ;
	}
	
	public static void logD(String tag, String message){
		Log.d(tag, message) ;
	}
	
	public static void logW(String tag, String message){
		Log.w(tag, message) ;
	}
	
}
