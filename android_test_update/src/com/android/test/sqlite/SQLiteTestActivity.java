package com.android.test.sqlite;

import java.util.List;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentValues;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.PopupMenu.OnMenuItemClickListener;
import android.widget.TextView;

import com.android.test.R;
import com.android.test.sqlite.db.MSQLiteOpenHelper;
import com.android.test.sqlite.db.User;

public class SQLiteTestActivity extends Activity {
	private TextView tRowId ; 
	private EditText name , age ;
	private Button save ;
	private ListView listView ;
	private ArrayAdapter<User> adapter ;
	
	private List<User> users;
	
	private MSQLiteOpenHelper mHelper = MSQLiteOpenHelper.getInstance(this) ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_sqlite);
		
		tRowId = (TextView)findViewById(R.id.rowId) ;
		
		name = (EditText)findViewById(R.id.name) ;
		age  = (EditText)findViewById(R.id.age ) ;
		
		save = (Button)findViewById(R.id.save) ;
		
		listView = (ListView)findViewById(R.id.listView1);
		
		save.setOnClickListener(new View.OnClickListener(){

			@Override
			public void onClick(View v) {
				String n = name.getText().toString().trim() ;
				String a = age .getText().toString().trim() ;
				ContentValues values = new ContentValues() ;
				values.put("name", n) ;
				values.put("age", Integer.valueOf(a)) ;
				long rowId = mHelper.insertOrUpdate("name = ? ", new String[]{n}, values) ;
				tRowId.setText(rowId+"") ;
				List<User> us = mHelper.queryUsers() ;
				users.clear() ;
				users.addAll(us) ;
				adapter.notifyDataSetChanged() ;
				name.setText("") ;
				age .setText("") ;
			}
			
		}) ;
		users = mHelper.queryUsers() ;
		adapter = new ArrayAdapter<User>(this, android.R.layout.simple_list_item_1, users) ;
		listView.setAdapter(adapter) ;
		listView.setOnItemClickListener(new OnItemClickListener() {
			@SuppressLint("NewApi")
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				final int pos = position ;
				 /** Instantiating PopupMenu class */
                PopupMenu popup = new PopupMenu(getBaseContext(), view);
 
                /** Adding menu items to the popumenu */
                popup.getMenuInflater().inflate(R.menu.menu_sqlite_popup, popup.getMenu());
 
                /** Defining menu item click listener for the popup menu */
                popup.setOnMenuItemClickListener(new OnMenuItemClickListener() {
 
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        if(R.id.delete == item.getItemId()){
                        	mHelper.delete(users.get(pos).name) ;
                        	tRowId.setText(users.get(pos).id+"") ;
                        	users.remove(pos) ;
                        	adapter.notifyDataSetChanged() ;
                        } ;
                        return true;
                    }
                });
 
                /** Showing the popup menu */
                popup.show();
			}
		}) ;
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

}
