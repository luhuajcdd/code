package com.android.test.sqlite.db;

public class User {
	public int id ;
	public String name ;
	public int age ;
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		return true;
	}



	@Override
	public String toString() {
		return " --> id = " +id + " ,  name = "+name + " , age = " + age;
	}
}
