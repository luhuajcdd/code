package com.android.test.sqlite.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class MSQLiteOpenHelper extends SQLiteOpenHelper {

	private static final String TABLE_NAME ="t_user";
	private static final String DATABASE_NAME ="USER.db";
	private static int VERSION = 1 ;
	private static SQLiteDatabase mDB;
	private static MSQLiteOpenHelper mHelper ;
	
	private MSQLiteOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, VERSION);
	}

	public static MSQLiteOpenHelper getInstance(Context context){
		if(mHelper == null){
			mHelper = new MSQLiteOpenHelper(context) ;
		}
		return mHelper ;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {

		db.execSQL("create table "+TABLE_NAME+"(_id integer primary key,name text, age integer)") ;
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}
	
	private SQLiteDatabase openReadable(){
		if(mDB != null){
			if(!mDB.isOpen()){
				mDB = this.getReadableDatabase() ;
			}else{
				return mDB ;
			}
		}else{
			mDB = this.getReadableDatabase() ;
		}
		return mDB ;
	}

	private SQLiteDatabase openWritable(){
		if(mDB != null){
			if(!mDB.isOpen()){
				mDB = this.getWritableDatabase() ;
			}else{
				return mDB ;
			}
		}else{
			mDB = this.getReadableDatabase() ;
		}
		return mDB ;
	}
	
	public long insertOrUpdate(String selection, String[] selectionArgs,ContentValues values){
		openWritable() ;
		Cursor cursor = null ;
		try{
			cursor = mDB.query(TABLE_NAME, null, selection, selectionArgs, null, null, null) ;
			long rowId = 0 ;
			if(cursor != null && cursor.getCount() > 0){ // update
				rowId = mDB.update(TABLE_NAME, values, selection, selectionArgs) ;
			}else {//insert
				rowId = mDB.insert(TABLE_NAME, null, values) ;
			}
			return rowId ;
		}finally{
			cursor.close() ;
			mDB.close() ;
		}
	}
	
	private Cursor query(){
		openReadable() ;
		Cursor cursor = null ;
		cursor = mDB.query(TABLE_NAME, null, null, null, null, null, null) ;
		return cursor;
	}
	
	public List<User> queryUsers(){
		List<User> users = new ArrayList<User>() ;
		Cursor cursor = query() ;
		try{
			if(cursor != null && cursor.getCount() > 0){
				while(cursor.moveToNext()){
					User user = new User() ;
					user.id = cursor.getInt(cursor.getColumnIndex("_id")) ;
					user.name = cursor.getString(cursor.getColumnIndex("name")) ;
					user.age = cursor.getInt(cursor.getColumnIndex("age")) ;
					users.add(user) ;
				}
			}
			return users ;
		}finally{
			cursor.close() ;
			mDB.close() ;
		}
	}
	
	public long delete(String name){
		openWritable() ;
		try{
			return mDB.delete(TABLE_NAME, "name = ? ", new String[]{name}) ;
		}finally{
			mDB.close() ;
		}
	}
	
}
