package com.designpatten.abstractfactory;

public interface Cat {

	void eatFish() ;
}
