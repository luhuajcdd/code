package com.designpatten.abstractfactory;

public class CreateFactoryWhite implements IAbstractFactory {

	@Override
	public Dog createDog() {
		return new WhiteDog();
	}

	@Override
	public Cat createCat() {
		return new WhiteCat();
	}

}
