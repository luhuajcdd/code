package com.designpatten.abstractfactory;

public class BlackDog implements Dog {

	@Override
	public void bitBone() {
		System.out.println("Black Dog  bit bone");
	}

}
