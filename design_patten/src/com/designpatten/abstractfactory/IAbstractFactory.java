package com.designpatten.abstractfactory;

public interface IAbstractFactory {

	Dog createDog() ;
	
	Cat createCat() ;
}
