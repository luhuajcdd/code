package com.designpatten.abstractfactory;

public class CreateFactoryBlack implements IAbstractFactory {

	@Override
	public Dog createDog() {
		return new BlackDog();
	}

	@Override
	public Cat createCat() {
		return new BlackCat();
	}

}
