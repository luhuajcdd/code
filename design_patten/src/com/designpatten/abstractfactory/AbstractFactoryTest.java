package com.designpatten.abstractfactory;

public class AbstractFactoryTest {
	public static void main(String[] args) {
		IAbstractFactory factory1 = new CreateFactoryWhite() ;
		factory1.createCat().eatFish() ; //White Cat eat fish
		factory1.createDog().bitBone() ; //White Dog  bit bone
		IAbstractFactory factory2 = new CreateFactoryBlack() ;
		factory2.createCat().eatFish() ; //Black Cat eat fish
		factory2.createDog().bitBone() ; //Black Dog  bit bone
	}
}

