package com.designpatten.abstractfactory;

public class WhiteCat implements Cat {

	@Override
	public void eatFish() {
		System.out.println("White Cat eat fish");
	}

}
