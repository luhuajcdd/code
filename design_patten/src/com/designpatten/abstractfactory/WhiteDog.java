package com.designpatten.abstractfactory;

public class WhiteDog implements Dog {

	@Override
	public void bitBone() {
		System.out.println("White Dog  bit bone");
	}

}
