package com.designpatten.abstractfactory;

public class BlackCat implements Cat {

	@Override
	public void eatFish() {
		System.out.println("Black Cat eat fish");
	}

}
