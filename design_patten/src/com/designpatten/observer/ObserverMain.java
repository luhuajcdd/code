package com.designpatten.observer;

public class ObserverMain {

	public static void main(String[] args) {
		ChildObservable observable = new ChildObservable() ;
		
		FatherObserver father = new FatherObserver() ;
		MotherObserver mother = new MotherObserver() ;
		
		observable.addObserver(father) ;
		observable.addObserver(mother) ;
		
		observable.changeT() ;
		if(observable.hasChanged()){
			observable.notifyObservers("sick...") ;
		}
		
	}
	
}
