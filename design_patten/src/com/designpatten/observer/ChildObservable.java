package com.designpatten.observer;

import java.util.Observable;

public class ChildObservable extends Observable {

	public void notifyObservers(String arg){
		super.notifyObservers(arg) ;
	}
	
	public void sick(){
		System.out.println(this.getClass().getName() + "------> sick()");
	}
	
	public void changeT(){
		super.setChanged() ;
	}
	
}
