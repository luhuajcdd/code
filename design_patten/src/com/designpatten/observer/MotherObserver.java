package com.designpatten.observer;

import java.util.Observable;
import java.util.Observer;

public class MotherObserver implements Observer {

	@Override
	public void update(Observable o, Object arg) {
		ChildObservable to = (ChildObservable)o ;
		to.sick() ;
		System.out.println("MotherObserver.update()=====" + arg);
	}

}
