package com.designpatten.builder;

public interface Car {
	
	void run() ;
	
}
