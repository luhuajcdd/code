package com.designpatten.builder;

public class Benz implements Car {

	@Override
	public void run() {
		System.out.println("Benz -------> run()");
	}

}
