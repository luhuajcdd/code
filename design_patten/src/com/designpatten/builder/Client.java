package com.designpatten.builder;

public class Client {
	public static void main(String[] args) {
		BenzBuilder benzBuilder = new BenzBuilder() ;
		BMWBuilder bmwBuilder = new BMWBuilder() ;
		
		Director benzDirector = new Director(benzBuilder) ;
		benzDirector.getCarTogether() ;
		Car benz = benzBuilder.getCar() ;
		benz.run() ;
		System.out.println("----------------------------");
		Director bmwDirector = new Director(bmwBuilder) ;
		bmwDirector.getCarTogether() ;
		Car bmw = bmwBuilder.getCar() ;
		bmw.run() ;
	}
}
