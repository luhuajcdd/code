package com.designpatten.builder;

public interface Builder {
	void buildCarWheel() ;
	void buildSteeringWheel() ;
	void buildEngine() ;
	void buildCarFrame() ;
	Car getCar() ;
}
