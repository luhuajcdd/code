package com.designpatten.adapter.classes;

public interface Target {

	void sampleOperation1() ;
	
	void sampleOperation2() ;
	
}
