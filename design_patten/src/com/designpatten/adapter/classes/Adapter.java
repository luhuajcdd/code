package com.designpatten.adapter.classes;

public class Adapter extends Adaptee implements Target {

	@Override
	public void sampleOperation2() {
		System.out.println("classes.Adapter--------------->sampleOperation2");
	}

}
