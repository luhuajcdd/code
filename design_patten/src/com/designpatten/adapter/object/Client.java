package com.designpatten.adapter.object;

public class Client {
	public static void main(String[] args) {
		
		Adapter adapter = new Adapter() ;
		adapter.setAdaptee(new Adaptee()) ;
		adapter.sampleOperation2() ;
		
	}
}
