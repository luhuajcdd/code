package com.designpatten.adapter.object;

public class Adapter implements Target {

	private Adaptee adaptee ;
	
	@Override
	public void sampleOperation2() {
		//实现Adaptee需要做的工作
		System.out.println("object.Adapter-------->sampleOperation2");
		adaptee.sampleOperation1() ;
	}

	@Override
	public void sampleOperation1() {
		// TODO Auto-generated method stub
		
	}
	
	public void setAdaptee( Adaptee adaptee ){
		this.adaptee = adaptee;
	}

}
