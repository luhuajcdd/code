package com.designpatten.classrelationship;

public class Association {

	class Computer{
		public void develop(){
			System.out.println("Develop ");
		}
	}
	
	class Person{
		private Computer computer ;
		
		public Person(Computer computer){
			this.computer = computer ;
		}
		
		public void work(){
			computer.develop() ;
			System.out.println("work");
		}
		
	}
	
}
