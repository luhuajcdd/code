package com.designpatten.classrelationship;

public class Composition {

	class Room{
		public Room createRoom(){
			System.out.println("��������");
			return new Room() ;
		}
	}
	
	class House{
		private Room room ;
		public House(){
			room = new Room() ;
		}
		
		public void createHouse(){
			room.createRoom() ;
			
		}
		
	}
	
}
