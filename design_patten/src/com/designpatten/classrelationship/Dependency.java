package com.designpatten.classrelationship;


public class Dependency {
	static class Car {
		public static void run(){
			System.out.println("汽车在奔跑");
		}
	}

	class Driver {
		//使用形参方式发生依赖关系
		public void toHome(Car car){
			car.run();
		}
		//使用局部变量发生依赖关系
		public void toOffice(){
			Car car = new Car();
			car.run();
		}
		//使用静态变量发生依赖关系
		public void goShopping(){
			Car.run();
		}
	}
	
	static class Boat{
		public static void row(){
			System.out.println("开动");
		}
	}
	
	class Person{
		public void crossRiver(Boat boat){
			boat.row() ;
		}
		
		public void fishing(){
			Boat boat = new Boat() ;
			boat.row() ;
		}
		
		public void patrol(){
			Boat.row() ;
		}
	}

}
